/** @file fkv_bbwa_tree.c
*
* @brief This module implements the binary balacing tree layer of the SLI FKV Key Value storage manager.
*
* @par
* Copyright © 2016 Sunrise Labs, Inc.
* All rights reserved.
*/

/*
FORMERLY Before conversion for use in SLI FKV:
Copyright (c) 2012-16 Doug Currie, Londonderry, NH, USA

Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without 
restriction, including without limitation the rights to use, copy, modify, merge, publish, 
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
References:

Implementing Sets Efficiently in a Functional Language
Stephen Adams
CSTR 92-10
Department of Electronics and Computer Science University of Southampton Southampton S09 5NH

Adams’ Trees Revisited Correct and Efficient Implementation
Milan Straka <fox@ucw.cz>
Department of Applied Mathematics Charles University in Prague, Czech Republic
*/

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "fkv_bbwa_cache.h"
#include "fkv_bbwa_tree.h"
#include "fkv_storage.h"
#include "fkv_context.h"
#include "fkv_hal.h"

/*
TODO
- check all calls to fkv_cache_node_locked for NULL? or should it always return something?
*/

static inline uint32_t fkv_noderef_size (fkv_context_t ctxt, fkv_noderef_t noderef)
{
    if (fkv_noderef_is_null(noderef))
    {
        return 0u;
    }
    else
    {
        const fkv_node_t *np = fkv_cache_node_unlocked(ctxt, noderef);
        return fkv_node_size(np);
    }
}

static inline int fkv_compare (uint8_t *a, uint8_t *b)
{
    int res = memcmp(a, b, sizeof(((fkv_node_t *)0)->key));

    // if (0 == res)
    // {
    //     // TODO if context has a deep_compare function, use it
    //     // but then we'd need additional arguments to this function:
    //     // fkv_context_t ctxt, uint8_t *deep_key, fkv_noderef_t noderef
    //     // So, better to call fkv_deep_compare from fkv_compare's call sites
    // }
    return res;
}

static inline fkv_noderef_t 
fkv_make_node (fkv_context_t ctxt, fkv_noderef_t left, uint8_t *key, fkv_noderef_t val, fkv_noderef_t right)
{
    uint32_t size = fkv_noderef_size(ctxt, left) + 1 + fkv_noderef_size(ctxt, right);

    return fkv_allocate_node(ctxt, key, val, size, left, right);
}

static inline fkv_noderef_t fkv_make_leaf (fkv_context_t ctxt, uint8_t *key, fkv_noderef_t val)
{
    return fkv_allocate_node(ctxt, key, val, 1, FKV_NULL_NODEREF, FKV_NULL_NODEREF);
}

/* Bounded Balance <omega,alpha> Tree
*/

static const int omega = 3; // 2.5 would require more arithmetic below
static const int alpha = 2; // 1.5 would match omega of 2.5, but require a delta parameter

/* **************************** balance ******************************** */

/*
singleL l k (Node rl _ rk rr) = node (node l k rl) rk rr
singleR (Node ll _ lk lr) k r = node ll lk (node lr k r)
doubleL l k (Node (Node rll _ rlk rlr) _ rk rr) =
  node (node l k rll) rlk (node rlr rk rr)
doubleR (Node ll _ lk (Node lrl _ lrk lrr)) k r =
  node (node ll lk lrl) lrk (node lrr k r)

balance left key right
    | size left + size right <= 1 = node left key right
    | size right > omega * size left + delta = case right of
         (Node rl _ _ rr) | size rl<alpha*size rr -> singleL left key right
                          | otherwise             -> doubleL left key right
    | size left > omega * size right + delta = case left of
         (Node ll _ _ lr) | size lr<alpha*size ll -> singleR left key right
                          | otherwise             -> doubleR left key right
    | otherwise = node left key right
*/

static fkv_noderef_t 
balance (fkv_context_t ctxt, fkv_noderef_t left, uint8_t *key, fkv_noderef_t val, fkv_noderef_t right)
{
    fkv_noderef_t res = FKV_NULL_NODEREF;

    uint32_t sl = fkv_noderef_size(ctxt, left);
    uint32_t sr = fkv_noderef_size(ctxt, right);

    if ((sl + sr) <= 1)
    {
        res = fkv_make_node(ctxt, left, key, val, right);
    }
    else if (sr > (omega * sl))
    {
        fkv_node_t *r = fkv_cache_node_locked(ctxt, right);

        if (fkv_noderef_size(ctxt, r->left) < (alpha * fkv_noderef_size(ctxt, r->right)))
        {
            // singleL
            res = fkv_make_node(ctxt, fkv_make_node(ctxt, left, key, val, r->left), r->key, r->val, r->right);
        }
        else
        {
            // doubleL
            fkv_node_t *rl = fkv_cache_node_locked(ctxt, r->left);
            res = fkv_make_node(ctxt, fkv_make_node(ctxt, left, key, val, rl->left),
                                rl->key, 
                                rl->val, 
                                fkv_make_node(ctxt, rl->right, r->key, r->val, r->right));

            fkv_uncache_node(ctxt, r->left, rl); /* uncache instead of unlock since node rl is no longer in tree */
        }
        fkv_uncache_node(ctxt, right, r); /* uncache instead of unlock since node right is no longer in tree */
    }
    else if (sl > (omega * sr))
    {
        fkv_node_t *l = fkv_cache_node_locked(ctxt, left);

        if (fkv_noderef_size(ctxt, l->right) < (alpha * fkv_noderef_size(ctxt, l->left)))
        {
            // singleR 
            res = fkv_make_node(ctxt, l->left, l->key, l->val, fkv_make_node(ctxt, l->right, key, val, right));
        }
        else
        {
            // doubleR
            fkv_node_t *lr = fkv_cache_node_locked(ctxt, l->right);
            res = fkv_make_node(ctxt, fkv_make_node(ctxt, l->left, l->key, l->val, lr->left),
                                lr->key,
                                lr->val,
                                fkv_make_node(ctxt, lr->right, key, val, right));
            fkv_uncache_node(ctxt, l->right, lr); /* uncache instead of unlock since node lr is no longer in tree */
        }
        fkv_uncache_node(ctxt, left, l); /* uncache instead of unlock since node left is no longer in tree */
    }
    else
    {
        res = fkv_make_node(ctxt, left, key, val, right);
    }
    return res;
}

/*
insert :: Ord a => a -> BBTree a -> BBTree a
insert k Nil = node Nil k Nil
insert k (Node left _ key right) = case k ‘compare‘ key of
                                     LT -> balance (insert k left) key right
                                     EQ -> node left k right
                                     GT -> balance left key (insert k right)
*/

fkv_noderef_t fkv_insert (fkv_context_t ctxt, fkv_noderef_t node, uint8_t *key, fkv_noderef_t val)
{
    fkv_noderef_t res = FKV_NULL_NODEREF;

    if (fkv_noderef_is_null(node))
    {
        res = fkv_make_leaf(ctxt, key, val);
    }
    else
    {
        fkv_node_t *nx = fkv_cache_node_locked(ctxt, node);

        int dif = fkv_compare(key, nx->key);

        if (dif < 0)
        {
            res = balance(ctxt, fkv_insert(ctxt, nx->left, key, val), nx->key, nx->val, nx->right);
        }
        else if (dif > 0)
        {
            res = balance(ctxt, nx->left, nx->key, nx->val, fkv_insert(ctxt, nx->right, key, val));
        }
        else // key and nx->key are eq
        {
            res = fkv_make_node(ctxt, nx->left, key, val, nx->right);
        }
        fkv_uncache_node(ctxt, node, nx); /* uncache instead of unlock since node is no longer in tree */
    }
    return res;
}

// nope is returned if key is not in tree
//
fkv_noderef_t fkv_lookup (fkv_context_t ctxt, fkv_noderef_t node, uint8_t *key, fkv_noderef_t nope)
{
    do
    {
        if (fkv_noderef_is_null(node))
        {
            return nope;
        }
        else
        {
            fkv_node_t *nx = fkv_cache_node_unlocked(ctxt, node);

            int dif = fkv_compare(key, nx->key);

            if (dif < 0)
            {
                node = nx->left;
            }
            else if (dif > 0)
            {
                node = nx->right;
            }
            else // key and nx->key are eq
            {
                return nx->val;
            }
        }
    } while (1);
}

// nope is returned if indexth item is not in tree
//
fkv_noderef_t fkv_indexth (fkv_context_t ctxt, fkv_noderef_t node, int32_t index, fkv_noderef_t nope)
{
    int32_t treesize = (int32_t )fkv_noderef_size(ctxt, node);
    uint32_t uindex;

    if ((index >= treesize) || (index < (-treesize)))
    {
        return nope; // index is out of range
    }
    else if (index < 0)
    {
        uindex = (uint32_t )(treesize + index); // when negative, reverse index from end rather than inorder
    }
    else
    {
        uindex = (uint32_t )index; // all set
    }

    do
    {
        // node is not null, or we'd have already returned

        fkv_node_t *nx = fkv_cache_node_locked(ctxt, node);

        uint32_t leftsize = fkv_noderef_size(ctxt, nx->left);

        fkv_unlock_node(ctxt, node, nx);

        if (uindex < leftsize)
        {
            node = nx->left;
        }
        else if (uindex > leftsize)
        {
            uindex -= leftsize + 1;
            node = nx->right;
        }
        else // we are there!
        {
            return nx->val;
        }

    } while (1);
}

static fkv_noderef_t find_min (fkv_context_t ctxt, fkv_noderef_t node)
{
    fkv_node_t *nx = fkv_cache_node_unlocked(ctxt, node);

    while (!fkv_noderef_is_null(nx->left))
    {
        node = nx->left;
        nx = fkv_cache_node_unlocked(ctxt, node);
    }
    return node;
}

// next_after is almost inorder successor, except it also works when the key is not present.
// It returns the value of the node with smallest nx->key > key
// nope is returned if there is no such node in the tree
//
fkv_noderef_t fkv_nafter (fkv_context_t ctxt, fkv_noderef_t node, uint8_t *key, fkv_noderef_t nope)
{
    int done = 0;
    fkv_noderef_t last_left_from = FKV_NULL_NODEREF;

    do
    {
        if (fkv_noderef_is_null(node))
        {
            node = last_left_from; // key not found
            done = 1;
        }
        else
        {
            fkv_node_t *nx = fkv_cache_node_unlocked(ctxt, node);

            int dif = fkv_compare(key, nx->key);

            if (dif < 0)
            {
                last_left_from = node;
                node = nx->left;
            }
            else if (dif > 0)
            {
                node = nx->right;
            }
            else // key and nx->key are eq
            {
                // return value from min node of right subtree, or from last_left_from if none
                //
                if (fkv_noderef_is_null(nx->right))
                {
                    node = last_left_from;
                }
                else
                {
                    node = find_min(ctxt, nx->right);
                }
                done = 1;
            }
        }
    } while (!done);

    if (fkv_noderef_is_null(node))
    {
        return nope; // no last_left_from and no right subtree
    }
    else
    {
        fkv_node_t *nx = fkv_cache_node_unlocked(ctxt, node);
        return nx->val;
    }
}

/* count_keys() is used in unit tests
*/
static inline uint32_t count_keys (fkv_context_t ctxt, fkv_noderef_t node, uint32_t count)
{
    do
    {
        if (fkv_noderef_is_null(node))
        {
            return count;
        }
        else
        {
            fkv_node_t *nx = fkv_cache_node_locked(ctxt, node);

            count += count_keys(ctxt, nx->right, 0);
            count += 1;
            node = nx->left;

            fkv_unlock_node(ctxt, node, nx);
        }
    } while (1);
}

/*
delete :: Ord a => a -> BBTree a -> BBTree a
delete _ Nil = Nil
delete k (Node left _ key right) = case k ‘compare‘ key of
                                     LT -> balance (delete k left) key right
                                     EQ -> glue left right
                                     GT -> balance left key (delete k right)
  where glue Nil right = right
        glue left Nil = left
        glue left right
          | size left > size right = let (key’, left’) = extractMax left
                                     in node left’ key’ right
          | otherwise              = let (key’, right’) = extractMin right
                                     in node left key’ right’
        extractMin (Node Nil _ key right) = (key, right)
        extractMin (Node left _ key right) = case extractMin left of
          (min, left’) -> (min, balance left’ key right)
        extractMax (Node left _ key Nil) = (key, left)
        extractMax (Node left _ key right) = case extractMax right of
          (max, right’) -> (max, balance left key right’)
*/

static fkv_noderef_t extract_min (fkv_context_t ctxt, fkv_noderef_t node, fkv_noderef_t *nodep)
{
    fkv_noderef_t res = FKV_NULL_NODEREF;

    fkv_node_t *nx = fkv_cache_node_locked(ctxt, node);

    if (fkv_noderef_is_null(nx->left))
    {
        *nodep = node;
        res = nx->right;
        fkv_unlock_node(ctxt, node, nx);
    }
    else
    {
        fkv_noderef_t leftp = extract_min(ctxt, nx->left, nodep);
        res = balance(ctxt, leftp, nx->key, nx->val, nx->right);
        fkv_uncache_node(ctxt, node, nx); /* uncache instead of unlock since node is no longer in tree */
    }
    return res;
}

static fkv_noderef_t extract_max (fkv_context_t ctxt, fkv_noderef_t node, fkv_noderef_t *nodep)
{
    fkv_noderef_t res = FKV_NULL_NODEREF;

    fkv_node_t *nx = fkv_cache_node_locked(ctxt, node);

    if (fkv_noderef_is_null(nx->right))
    {
        *nodep = node;
        res = nx->left;
        fkv_unlock_node(ctxt, node, nx);
    }
    else
    {
        fkv_noderef_t rightp = extract_max(ctxt, nx->right, nodep);
        res = balance(ctxt, nx->left, nx->key, nx->val, rightp);
        fkv_uncache_node(ctxt, node, nx); /* uncache instead of unlock since node is no longer in tree */
    }
    return res;
}

static fkv_noderef_t glue (fkv_context_t ctxt, fkv_noderef_t left, fkv_noderef_t right)
{
    fkv_noderef_t res = FKV_NULL_NODEREF;
    fkv_node_t *nx;

    if (fkv_noderef_is_null(left))
    {
        res = right;
    }
    else if (fkv_noderef_is_null(right))
    {
        res = left;
    }
    else if (fkv_noderef_size(ctxt, left) > fkv_noderef_size(ctxt, right))
    {
        fkv_noderef_t nodep;
        fkv_noderef_t leftp = extract_max(ctxt, left, &nodep);

        nx = fkv_cache_node_locked(ctxt, nodep);
        res = fkv_make_node(ctxt, leftp, nx->key, nx->val, right);
        fkv_uncache_node(ctxt, nodep, nx); /* uncache instead of unlock since node is no longer in tree */
    }
    else
    {
        fkv_noderef_t nodep;
        fkv_noderef_t rightp = extract_min(ctxt, right, &nodep);

        nx = fkv_cache_node_locked(ctxt, nodep);
        res = fkv_make_node(ctxt, left, nx->key, nx->val, rightp);
        fkv_uncache_node(ctxt, nodep, nx); /* uncache instead of unlock since node is no longer in tree */
    }
    return res;
}

fkv_noderef_t fkv_delete (fkv_context_t ctxt, fkv_noderef_t node, uint8_t *key)
{
    fkv_noderef_t res = FKV_NULL_NODEREF;

    if (fkv_noderef_is_null(node))
    {
        res = node;
    }
    else
    {
        fkv_node_t *nx = fkv_cache_node_locked(ctxt, node);

        int dif = fkv_compare(key, nx->key);

        if (dif < 0)
        {
            res = balance(ctxt, fkv_delete(ctxt, nx->left, key), nx->key, nx->val, nx->right);
        }
        else if (dif > 0)
        {
            res = balance(ctxt, nx->left, nx->key, nx->val, fkv_delete(ctxt, nx->right, key));
        }
        else // key and nx->key are eq
        {
            res = glue(ctxt, nx->left, nx->right);
        }

        fkv_uncache_node(ctxt, node, nx); /* uncache instead of unlock since node is no longer in tree */
    }
    return res;
}

/* ***************************************** vvv TODO vvv *********************************************** */

#if 0

/* The heap memory required by these operations depend on the depth of the tree.
**
** For a bbwa tree, the max depth is log2(n) / log2(1 + 1/ω)
** With omega at 3, log2(1 + 1/ω) = log2(1 + 1/3) = 0.41503749927884
** The upper bound on depth is 2.41 * log2(n); we use 2.5 to stick with integer arithmetic
**
** delete can make about 3 nodes per level in the tree
** insert can make about 3 nodes per level in the tree
**
** Here is the calculation...
*/

static cell_t cells_needed_conservatively (fkv_noderef_t tree, cell_t factor)
{
    cell_t conservative_tsize = fkv_noderef_size(tree) * 5u;
#if UINTPTR_MAX > (2ull ^ 32 - 1)
    cell_t conservative_depth = (64u - __builtin_clzll(conservative_tsize));
#else
    cell_t conservative_depth = (32u - __builtin_clzll(conservative_tsize));
#endif
    // multiply by 2.5
    conservative_depth = 2 * conservative_depth + conservative_depth / 2;
    return factor * conservative_depth;
}

#endif

