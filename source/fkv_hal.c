/* fkv_hal.c
*/
#include "fkv_context.h"
#include "fkv_hal.h"
#include "fkv_api.h"
#include "fkv_storage.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <io.h>
#include <share.h>
#include <windows.h>
#include <winioctl.h>

WCHAR logical_drive[50];

//Constants
#define SD_DATATIMEOUT ((uint32_t)100000000)

//Prototypes

//Private Variables
static uint8_t read_buffer[FKV_BLOCK_SIZE];
static uint8_t write_buffer[FKV_BLOCK_SIZE];

static HANDLE fh = NULL;

void fkv_hal_open(void)
{
    if (fh == NULL) 
    {                    
        DWORD retBytes;
        static HANDLE temp_fh;
                      
        temp_fh = CreateFile(logical_drive, 
                             GENERIC_READ | GENERIC_WRITE, 
                             FILE_SHARE_READ | FILE_SHARE_WRITE, 
                             NULL, 
                             OPEN_EXISTING, 
                             FILE_ATTRIBUTE_NORMAL, 
                             NULL);        
        if(temp_fh == INVALID_HANDLE_VALUE) 
        {
            fprintf(stderr,"Open failed on input file: %d\n", GetLastError());
            fkv_panic();
        }

        VOLUME_DISK_EXTENTS vde;
        if (DeviceIoControl(temp_fh, 
                            IOCTL_VOLUME_GET_VOLUME_DISK_EXTENTS,
                            NULL, 
                            0, 
                            &vde, 
                            sizeof(vde), 
                            &retBytes, 
                            NULL) == 0)
        {
            fprintf(stderr, "Failed to get device disk extents: %d\n", GetLastError());
            fkv_panic();
        }
        CloseHandle(temp_fh);

        LPCWSTR physical_drive[50];
        wsprintf(physical_drive, L"\\\\.\\PhysicalDrive%d", vde.Extents[0].DiskNumber);
        fh = CreateFile(physical_drive, 
                        MAXIMUM_ALLOWED, 
                        FILE_SHARE_READ | FILE_SHARE_WRITE, 
                        NULL, 
                        OPEN_EXISTING, 
                        FILE_FLAG_NO_BUFFERING | FILE_FLAG_WRITE_THROUGH, 
                        NULL);
        if(fh == INVALID_HANDLE_VALUE) // this may happen if another program is already reading from disk
        {
            fprintf(stderr,"Open failed on input file: %d\n", GetLastError());
            fkv_panic();
        }  
    }   
}

void fkv_hal_close(void)
{
    if (fh != NULL)
    {
        CloseHandle(fh);
        fh = NULL;
    }
}

void fkv_device_info (fkv_device_info_t *dev_info)
{    
    DWORD retBytes;
    DISK_GEOMETRY_EX disk_geometry;
    if( DeviceIoControl(fh, 
                        0x700a0, //IOCTL_DISK_GET_DRIVE_GEOMETRY_EX
                        NULL, 
                        0,
                        &disk_geometry, 
                        sizeof(disk_geometry), 
                        &retBytes, 
                        NULL) == 0)
    {
        fprintf(stderr, "Failed to get device capacity: %d\n", GetLastError());
        fkv_panic();
    }
    dev_info->capacity_bytes = (uint64_t)disk_geometry.DiskSize.QuadPart;
}

void fkv_hal_write(uint32_t *write_buffer, uint64_t write_buffer_size, const uint64_t write_address)
{
    LARGE_INTEGER win_write_address;
    win_write_address.QuadPart = write_address;
    DWORD written_bytes;
    if((write_buffer_size % FKV_BLOCK_SIZE) != 0)
    {
        fprintf(stderr, "Invalid Write Buffer Size: Not divisable by block size\n");
        fkv_panic();
    }  
    
    if((write_address % FKV_BLOCK_SIZE) != 0)
    {
        fprintf(stderr, "Invalid Write Address: Not divisable by block size\n");
        fkv_panic();
    }  

    if (SetFilePointerEx(fh, win_write_address, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
    {        
        fprintf(stderr, "Failed to seek to page position during write\n");
        fkv_panic();
    }

    if (WriteFile(fh, write_buffer, write_buffer_size, &written_bytes, 0) == FALSE)
    {        
        fprintf(stderr, "Failed to write: %d\n", GetLastError());
        fkv_panic();
    }    
}

void fkv_hal_read(uint32_t *read_buffer, uint64_t read_buffer_size, const uint64_t read_address)
{ 
    LARGE_INTEGER win_read_address;
    win_read_address.QuadPart = read_address;
    DWORD read_bytes;
    if((read_buffer_size % FKV_BLOCK_SIZE) != 0)
    {
        fprintf(stderr, "Invalid Read Buffer Size: Not divisable by block size\n");
        fkv_panic();
    }
	
	if((read_address % FKV_BLOCK_SIZE) != 0)
    {
        fprintf(stderr, "Invalid Read Address: Not divisable by block size\n");
        fkv_panic();
    }  
      
    if (SetFilePointerEx(fh, win_read_address, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
    {        
        fprintf(stderr, "Failed to seek to page position during read");
        fkv_panic();
    }

    if (ReadFile(fh, read_buffer, read_buffer_size, &read_bytes, 0) == FALSE)
    {        
        fprintf(stderr, "Failed to read");
        fkv_panic();
    }    
}

void fkv_hal_erase(const uint64_t erase_address, uint64_t erase_size)
{
#if 0
    DWORD returned = 0;    

    int device_cmd_data_size = sizeof(SFFDISK_DEVICE_COMMAND_DATA);
    int sd_cmd_description_size = sizeof(SDCMD_DESCRIPTOR);
    int data_size = 4;
    int total_size = device_cmd_data_size + sd_cmd_description_size + data_size;
    //printf("device_cmd_data_size = %d\n", device_cmd_data_size);
    //printf("sd_cmd_description_size = %d\n", sd_cmd_description_size);
    //printf("data_size = %d\n", data_size);
    //printf("total_size = %d\n", total_size);
    PSFFDISK_DEVICE_COMMAND_DATA data = malloc(total_size);
    if (!data) 
    {
        printf("malloc failed\n");
        fkv_panic();
    }
    memset(data, 0, total_size);
    data->HeaderSize = sizeof(SFFDISK_DEVICE_COMMAND_DATA);
    data->Command = SFFDISK_DC_DEVICE_COMMAND;
    data->ProtocolArgumentSize = sizeof(SDCMD_DESCRIPTOR);
    data->DeviceDataBufferSize = data_size;    
    PSDCMD_DESCRIPTOR desc = (PSDCMD_DESCRIPTOR)data->Data;    
    desc->CmdClass = SDCC_STANDARD; 
    desc->TransferDirection = SDTD_READ;
    desc->TransferType = SDTT_CMD_ONLY;                

    data->Data[sd_cmd_description_size] = (uint32_t)(erase_address / FKV_BLOCK_SIZE); /* Erase start address in block size */
    data->Information = (uint32_t)(erase_address / FKV_BLOCK_SIZE); /* Erase start address in block size */
    desc->Cmd = 32; //SD_CMD_SD_ERASE_GRP_END
    desc->ResponseType = SDRT_1;  
    int ret = DeviceIoControl(fh, IOCTL_SFFDISK_DEVICE_COMMAND, data, total_size, data, total_size, &returned, NULL);
    if (!ret) 
    {
        printf("ioctl failed: %d\n", GetLastError());
        fkv_panic();
    }  

    data->Data[sd_cmd_description_size] = (uint32_t)(erase_address / FKV_BLOCK_SIZE) + (uint32_t)(erase_size / FKV_BLOCK_SIZE);
    data->Information = (uint32_t)(erase_address / FKV_BLOCK_SIZE) + (uint32_t)(erase_size / FKV_BLOCK_SIZE); /* Erase end address */
    desc->Cmd = 33; //SD_CMD_SD_ERASE_GRP_END
    ret = DeviceIoControl(fh, IOCTL_SFFDISK_DEVICE_COMMAND, data, total_size, data, total_size, &returned, NULL);
    if (!ret) 
    {
        printf("ioctl failed: %d\n", GetLastError());
        fkv_panic();
    } 
    //hexDump("Erase end Data", (void*)data, total_size); 
     
    data->Information = 0; /* None */
    desc->Cmd = 38; /* run erase */
    desc->ResponseType = SDRT_1B; 
    ret = DeviceIoControl(fh, IOCTL_SFFDISK_DEVICE_COMMAND, data, total_size, data, total_size, &returned, NULL);
    if (!ret) 
    {
        printf("ioctl failed: %d\n", GetLastError());
        fkv_panic();
    } 
    //hexDump("Erase Data", (void*)data, total_size); 
    
    free(data);
#endif
}

void fkv_hal_read_commit_record(fkv_commit_record_t *commit_rec)
{    
    //fkv_hal_read((uint32_t *)read_buffer, FKV_BLOCK_SIZE, (uint64_t)FKV_COMMIT_RECORD_ADDRESS);      

    //memcpy(commit_rec, &read_buffer, sizeof(fkv_commit_record_t));
}

void fkv_hal_write_commit_record(fkv_commit_record_t *commit_rec)
{    
    //memcpy(write_buffer, commit_rec, sizeof(fkv_commit_record_t));
    
    //fkv_hal_write((uint32_t *)write_buffer, FKV_BLOCK_SIZE, (uint64_t)FKV_COMMIT_RECORD_ADDRESS);         
}

//error
void fkv_panic (void)
{
    exit(-1);
}