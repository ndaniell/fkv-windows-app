/** @file fkv_storage.c
*
* @brief This module implements the storage layer of the SLI FKV Key Value storage manager.
*
* @par
* Copyright © 2016 Sunrise Labs, Inc.
* All rights reserved.
*/

#include <stdio.h>
#include <string.h>

#include "fkv_bbwa_tree.h"
#include "fkv_storage.h"
#include "fkv_context.h"
#include "fkv_hal.h"
#include "fkv_sha1.h"

// debug 
uint8_t verbose_storage_debug = 0u;

#if FKV_TEST_HAL_WRITE_PAGE_BUFFER
uint8_t fvk_prev_op[4];
static inline void fkv_record_op (uint8_t op)
{
    fvk_prev_op[3] = fvk_prev_op[2];
    fvk_prev_op[2] = fvk_prev_op[1];
    fvk_prev_op[1] = fvk_prev_op[0];
    fvk_prev_op[0] = op;
}
static inline void fkv_record_subop (uint8_t op)
{
    fvk_prev_op[0] = op;
}
#else
#define fkv_record_op(x)    /* nothing */
#define fkv_record_subop(x) /* nothing */
#endif

static uint8_t  cached_node_block[FKV_BLOCK_SIZE];    
static uint32_t cached_node_block_number;

static inline bool fkv_node_is_at_page_boundary (fkv_context_t ctxt, fkv_noderef_t noderef)
{
    return ((noderef % FKV_NODES_PER_PAGE) == 0u);
}

static inline bool fkv_node_is_at_storage_end (fkv_context_t ctxt, fkv_noderef_t noderef)
{
    return ((noderef / FKV_NODES_PER_PAGE) >= ctxt->size_in_pages);
}

static inline bool fkv_node_is_in_page_buffer (fkv_context_t ctxt, fkv_noderef_t noderef)
{
    /* next_node should never be zero, but we'll check anyway */
    return ((noderef / FKV_NODES_PER_PAGE)
             == (((ctxt->commit_record.next_node == 0u) ? 0u : (ctxt->commit_record.next_node - 1u)) / FKV_NODES_PER_PAGE));
}

static inline fkv_noderef_t fkv_increment_noderef (fkv_context_t ctxt, fkv_noderef_t noderef, uint32_t incr)
{
    return (noderef + incr) % (ctxt->size_in_pages * FKV_NODES_PER_PAGE);
}

static void fkv_read_page_buffer (fkv_context_t ctxt, fkv_noderef_t noderef)
{
    if (0 != (noderef % FKV_NODES_PER_PAGE))
    {
        fprintf(stderr, "ERROR: fkv_hal_read_page_buffer %u is not on a page boundary!\n", noderef);
        fkv_panic();
    }

    uint32_t base = (ctxt->base_page * FKV_PAGESIZE); //where to read from 
        
    fkv_hal_read((uint32_t *)ctxt->page_buffer, FKV_PAGESIZE, (base + (noderef * sizeof(fkv_node_t))));  
} 
    
static void fkv_write_page_buffer (fkv_context_t ctxt)
{   
#if FKV_TEST_HAL_WRITE_PAGE_BUFFER
    extern uint8_t fvk_prev_op[];
    static uint32_t last_page_written = 0u - 1u; // the first written node will be zero during unit test
#endif
    uint32_t pagenum = fkv_page_number_of_page_buffer(ctxt); 
    uint64_t pageadr = (uint64_t)(((ctxt->base_page + pagenum) * FKV_PAGESIZE)); 

    if((pageadr % FKV_ERASE_SIZE) == 0)
    {
        if ((pagenum + (FKV_ERASE_SIZE / FKV_PAGESIZE)) > ctxt->size_in_pages)
        {
            // The erase size extends past the end of the partition
            // So erase from the page address to the end of the partition
            uint64_t bytes_to_end = ((uint64_t)(ctxt->size_in_pages - pagenum) * FKV_PAGESIZE);
            fkv_hal_erase(pageadr, bytes_to_end);
            // Then erase the remaining number of pages from the beginning
            fkv_hal_erase(ctxt->base_page * FKV_PAGESIZE, (FKV_ERASE_SIZE - bytes_to_end));
        }
        else
        {
            //The erase does not wrap so erase the erase size.
            fkv_hal_erase(pageadr, FKV_ERASE_SIZE);
        }
    }
    
    fkv_hal_write((uint32_t *)ctxt->page_buffer, FKV_PAGESIZE, pageadr);
      
    ctxt->page_buffer_is_dirty = false; 
    
#if FKV_TEST_HAL_WRITE_PAGE_BUFFER
    if (pagenum == ((last_page_written + 1u) % ctxt->size_in_pages)) // unit testing
    {
        // ok
    }
    else
    {
        fprintf(stderr, 
                "ERROR fkv_write_page_buffer last: %u this: %u prevops: %u,%u,%u,%u\n", 
                last_page_written, 
                pagenum, 
                fvk_prev_op[3], 
                fvk_prev_op[2], 
                fvk_prev_op[1], 
                fvk_prev_op[0]);
    }
    last_page_written = pagenum;
#endif
} 


/* external functions */

#if defined(FKV_OPTION_REMOVEABLE_RECOVERABLE)
void fkv_page_fill(fkv_context_t ctxt)
{
    //Fill the remaining page buffer with 1's to accommodate the recovery algorithm
    uint32_t page_buffer_next_node = ctxt->commit_record.next_node % FKV_NODES_PER_PAGE;   
    if(page_buffer_next_node > 0)
    {
        memset(&((fkv_node_t *)ctxt->page_buffer)[page_buffer_next_node], 
               0xFFu, 
               sizeof(fkv_node_t) * (FKV_NODES_PER_PAGE - page_buffer_next_node));
    }  
}
#endif

fkv_noderef_t fkv_allocate_node(fkv_context_t ctxt
                              , uint8_t *key
                              , fkv_noderef_t val
                              , uint32_t size
                              , fkv_noderef_t left
                              , fkv_noderef_t right)
{
    fkv_noderef_t next_node = ctxt->commit_record.next_node;

    fkv_record_op(1u);

    // if at a page boundary, flush the page buffer, and see if we need to wrap
    //
    if (fkv_node_is_at_page_boundary(ctxt, next_node))
    {
        if (ctxt->page_buffer_is_dirty)
        {
            fkv_write_page_buffer(ctxt);
        }
        else
        {
            // just use page buffer as-is
        }
        if (fkv_node_is_at_storage_end(ctxt, next_node))
        {
            // wrap to the beginning of the storage
            next_node = 0u; // note that zero is FKV_NULL_NODE but we skip that slot below
        }
        else
        {
            // no need to wrap
        }
        memset(ctxt->page_buffer, 0u, sizeof(fkv_node_t)); // zero the skipped node
#ifdef FKV_OPTION_REMOVEABLE_RECOVERABLE        
        ((fkv_record_header_t *)(ctxt->page_buffer))->commit_record.sequence_number = ctxt->commit_record.sequence_number;            
#endif    
        next_node += 1u;
    }

    fkv_node_t *pnode = &((fkv_node_t *)ctxt->page_buffer)[next_node % FKV_NODES_PER_PAGE];

    memcpy(pnode->key, key, sizeof(pnode->key));
    pnode->val = val;
    pnode->left = left;
    pnode->right = right;
    fkv_set_node_size(pnode, size);

    ctxt->page_buffer_is_dirty = true;

    ctxt->commit_record.next_node = next_node + 1u;

    return next_node;
}

void fkv_read_node(fkv_context_t ctxt, fkv_node_t *pnode_target, fkv_noderef_t noderef)
{
    if (ctxt->page_buffer_is_dirty && fkv_node_is_in_page_buffer(ctxt, noderef))
    {
        // if still in buffer, copy from page buffer
        memcpy(pnode_target, 
               &((fkv_node_t *)ctxt->page_buffer)[noderef % FKV_NODES_PER_PAGE], 
               sizeof(fkv_node_t));
    }
    else
    {
        uint32_t base = (ctxt->base_page * FKV_PAGESIZE); //where to read from
        uint32_t offs = (noderef * sizeof(fkv_node_t));
        uint32_t offs_rem   = offs % (uint32_t )FKV_BLOCK_SIZE;
        uint32_t offs_floor = offs - offs_rem;
            
        // if the node is in the current cached node block
        // don't bother getting it from storage. 
        if ((offs_floor != cached_node_block_number) || (ctxt->node_block_cache_populated == 0))
        { 
            // If the node is not in the node cache get it from the persistent
            // storage and save it in the cache                  
            fkv_hal_read((uint32_t *)cached_node_block, FKV_BLOCK_SIZE, (uint64_t )base + offs_floor);  
            cached_node_block_number = offs_floor;
            ctxt->node_block_cache_populated = 1;
        }
        
        memcpy(pnode_target, &cached_node_block[offs_rem], sizeof(fkv_node_t));  
    }
}

fkv_noderef_t
fkv_allocate_kvir (fkv_context_t ctxt
                 , uint8_t *key             /* in */
                 , uint16_t key_size        /* in */
                 , uint16_t flags           /* in; flags are needed so we know how to convert Key to tree key */
                 , uint8_t *value           /* in */
                 , uint16_t value_size      /* in */
                 )
{
    fkv_noderef_t next_node = ctxt->commit_record.next_node;
    fkv_noderef_t res = FKV_NULL_NODEREF;

    fkv_record_op(2u);

    if (ctxt->page_buffer_is_dirty)
    {
        fkv_write_page_buffer(ctxt);
    }
    else
    {
        // just use page buffer as-is
    }
    // top up next_node if it's not at the start of a page
    //
    if (fkv_node_is_at_page_boundary(ctxt, next_node))
    {
        // all set
    }
    else
    {
        next_node = (fkv_page_number_of_page_buffer(ctxt) + 1u) * FKV_NODES_PER_PAGE;
    }
    if (fkv_node_is_at_storage_end(ctxt, next_node))
    {
        // wrap to the beginning of the storage
        next_node = 0u;
    }
    else
    {
        // no need to wrap
    }

    fkv_record_subop(21u);

    res = next_node; // this is where we're putting the record!

    next_node += 1u; // this makes fkv_page_number_of_page_buffer() and fkv_hal_write_page_buffer() work correctly
    ctxt->commit_record.next_node = next_node;

    // set up the header
    ((fkv_record_header_t *)(ctxt->page_buffer))->flags    = flags;
    ((fkv_record_header_t *)(ctxt->page_buffer))->key_size = key_size;
    ((fkv_record_header_t *)(ctxt->page_buffer))->val_size = value_size;
    
#ifdef FKV_OPTION_REMOVEABLE_RECOVERABLE
    ctxt->commit_record.sequence_number++;
    ((fkv_record_header_t *)(ctxt->page_buffer))->commit_record = ctxt->commit_record;    
    ((fkv_record_header_t *)(ctxt->page_buffer))->signature = FKV_RECORD_SIGNATURE;    
#endif    

    uint16_t pgidx = sizeof(fkv_record_header_t);
    uint16_t fmidx = 0u;
    // copy the key
    while (key_size > fmidx)
    {
        if ((key_size - fmidx) >= (FKV_PAGESIZE - pgidx)) // should never happen since we now limit key size
        {
            memcpy(&ctxt->page_buffer[pgidx], &key[fmidx], FKV_PAGESIZE - pgidx);
            fmidx += (FKV_PAGESIZE - pgidx);
            pgidx = 0u;
            // page is full, flush
            fkv_write_page_buffer(ctxt);
            next_node = fkv_increment_noderef(ctxt, next_node, FKV_NODES_PER_PAGE);
            ctxt->commit_record.next_node = next_node;
        }
        else
        {
            memcpy(&ctxt->page_buffer[pgidx], &key[fmidx], key_size - fmidx);
            pgidx += key_size - fmidx;
            fmidx = key_size; // will break the loop
            // page is not full, but dirty
            ctxt->page_buffer_is_dirty = true;
        }
    }

    fkv_record_subop(22u);

    fmidx = 0u;
    // copy the value
    while (value_size > fmidx)
    {
        if ((value_size - fmidx) >= (FKV_PAGESIZE - pgidx))
        {
            memcpy(&ctxt->page_buffer[pgidx], &value[fmidx], FKV_PAGESIZE - pgidx);
            fmidx += (FKV_PAGESIZE - pgidx);
            pgidx = 0u;
            // page is full, flush
            fkv_write_page_buffer(ctxt);
            next_node = fkv_increment_noderef(ctxt, next_node, FKV_NODES_PER_PAGE);
            ctxt->commit_record.next_node = next_node;
        }
        else
        {
            memcpy(&ctxt->page_buffer[pgidx], &value[fmidx], value_size - fmidx);
            pgidx += value_size - fmidx;
            fmidx = value_size; // will break the loop
            // page is not full, but dirty
            ctxt->page_buffer_is_dirty = true;
        }
    }          

    // adjust next_node by rounding up to beyond data
    //
    next_node += ((pgidx + (sizeof(fkv_node_t) - 1u)) / sizeof(fkv_node_t)) - 1u; // -1 to compensate for + 1u above
    ctxt->commit_record.next_node = next_node;

#if defined(FKV_OPTION_REMOVEABLE_RECOVERABLE)
    if ((0u < pgidx) && (pgidx < sizeof(fkv_record_header_t)))
    {
        memset(&ctxt->page_buffer[pgidx], 0u, sizeof(fkv_record_header_t) - pgidx);
    }
#endif

    fkv_record_subop(23u);

    // flush page if we've reached the end by rounding up, in which case, zero the next header
    //
    if (fkv_node_is_at_page_boundary(ctxt, next_node))
    {
        if (ctxt->page_buffer_is_dirty)
        {
            fkv_write_page_buffer(ctxt);
        }
        else
        {
            // no need to write
        }

        if (fkv_node_is_at_storage_end(ctxt, next_node))
        {
            // wrap to the beginning of the storage
            next_node = 0u;
        }
        else
        {
            // no need to wrap
        }
        // zero the next header so recovery software will know this is an Index page
        memset(ctxt->page_buffer, 0u, sizeof(fkv_node_t)); // zero the skipped node
        next_node += 1u;

        ctxt->commit_record.next_node = next_node;
        ctxt->page_buffer_is_dirty = true;
    }
    else
    {
        // all set
    }
    fkv_record_subop(24u);

    return res;
}

void
fkv_read_kvir    (fkv_context_t ctxt  
                 , uint8_t  *key            /* out; NULL pointer ok if key not wanted */
                 , uint16_t *pkey_size      /* in/out; in: size of key buffer, zero ok; out: size of key */
                 , uint8_t  *value          /* out; NULL pointer ok if value not wanted */
                 , uint16_t *pvalue_size    /* in/out; in: size of value buffer, zero ok; out: size of value */
                 , fkv_noderef_t noderef    /* in */
                 )
{
    uint16_t key_size   = (pkey_size   == NULL) ? 0 : *pkey_size;
    uint16_t value_size = (pvalue_size == NULL) ? 0 : *pvalue_size;

    fkv_record_op(3u);

    if (ctxt->page_buffer_is_dirty)
    {
        fkv_write_page_buffer(ctxt);
    }
    else
    {
        // just use page buffer as-is
    }
    fkv_read_page_buffer(ctxt, noderef);
    noderef = fkv_increment_noderef(ctxt, noderef, FKV_NODES_PER_PAGE);

    uint16_t key_size_stored   = ((fkv_record_header_t *)(ctxt->page_buffer))->key_size;
    uint16_t value_size_stored = ((fkv_record_header_t *)(ctxt->page_buffer))->val_size;

    if (pkey_size  == NULL)  { /* punt */ } else { *pkey_size   = key_size_stored; }
    if (pvalue_size == NULL) { /* punt */ } else { *pvalue_size = value_size_stored; }

    uint16_t pgidx = sizeof(fkv_record_header_t);
    uint16_t toidx = 0u;

    if (key == NULL)
    {
        // skip key
    }
    else
    {
        // copy the key
        uint16_t copy_size = (key_size < key_size_stored) ? key_size : key_size_stored;

        while (copy_size > toidx)
        {
            if ((copy_size - toidx) >= (FKV_PAGESIZE - pgidx)) // should never happen since we now limit key size
            {
                memcpy(&key[toidx], &ctxt->page_buffer[pgidx], FKV_PAGESIZE - pgidx);
                toidx += (FKV_PAGESIZE - pgidx);
                pgidx = 0u;
                // page is exhausted, refresh
                // Note: this will be an unnecessary read if the key of completely read and the value is not needed.
                fkv_read_page_buffer(ctxt, noderef);
                noderef = fkv_increment_noderef(ctxt, noderef, FKV_NODES_PER_PAGE);
            }
            else
            {
                memcpy(&key[toidx], &ctxt->page_buffer[pgidx], copy_size - toidx);
                pgidx += copy_size - toidx;
                toidx = copy_size; // will break the loop
            }
        }
    }
    // now skip past any unread key bytes
    //
    if (key_size_stored > toidx)
    {
        if ((key_size_stored - toidx) > (FKV_PAGESIZE - pgidx)) // should never happen since we now limit key size
        {
            toidx += (FKV_PAGESIZE - pgidx);
            //pgidx = 0u;
            //noderef++;  // defer to below
            pgidx          = (key_size_stored - toidx) % FKV_PAGESIZE;
            uint32_t pages = (key_size_stored - toidx) / FKV_PAGESIZE;
            noderef = fkv_increment_noderef(ctxt, noderef, pages * FKV_NODES_PER_PAGE);
            fkv_read_page_buffer(ctxt, noderef);
            noderef = fkv_increment_noderef(ctxt, noderef, FKV_NODES_PER_PAGE);
        }
        else
        {
            pgidx += key_size_stored - toidx;
        }
    }
    else
    {
        // done
    }
    if (value == NULL)
    {
        // skip value
    }
    else
    {
        // copy the value
        toidx = 0u;
        uint16_t copy_size = (value_size < value_size_stored) ? value_size : value_size_stored;

        while (copy_size > toidx)
        {
            if ((copy_size - toidx) >= (FKV_PAGESIZE - pgidx))
            {
                memcpy(&value[toidx], &ctxt->page_buffer[pgidx], FKV_PAGESIZE - pgidx);
                toidx += (FKV_PAGESIZE - pgidx);
                pgidx = 0u;
                // page is exhausted, refresh?
                if (copy_size > toidx)
                {
                    fkv_read_page_buffer(ctxt, noderef);
                    noderef = fkv_increment_noderef(ctxt, noderef, FKV_NODES_PER_PAGE);
                }
                else
                {
                    // all done, punt the extra read
                }
            }
            else
            {
                memcpy(&value[toidx], &ctxt->page_buffer[pgidx], copy_size - toidx);
                pgidx += copy_size - toidx;
                toidx = copy_size; // will break the loop
            }
        }
    }
}

void fkv_commit (fkv_context_t ctxt)
{
    fkv_record_op(4u);

    if (ctxt->page_buffer_is_dirty)
    {
        fkv_write_page_buffer(ctxt);
    }
    else
    {
        // don't bother writing clean buffer
    }    
    
    fkv_hal_write_commit_record(&ctxt->commit_record);    
}

#ifdef FKV_OPTION_REMOVEABLE_RECOVERABLE

// The recovery code only works when FKV_OPTION_REMOVEABLE_RECOVERABLE is enabled

static inline int8_t fkv_valid_record(fkv_context_t ctxt, fkv_record_header_t *record_header)
{
    return ((record_header->signature == FKV_RECORD_SIGNATURE) &&
            (record_header->commit_record.log_start_page <= ctxt->size_in_pages) &&
            (record_header->commit_record.next_node <= (ctxt->size_in_pages * FKV_NODES_PER_PAGE)) &&
            (record_header->key_size != 0u)) ? 1 : 0;
}             

// Note: we require that the hole < 50% partition size, unless the partition hasn't wrapped yet
//
// Note: the page_num used as arguments and return values may be from 0 to 2 * ctxt->size_in_pages
// We add ctxt->size_in_pages to keep low < high even when high wraps the end of the circular store.
//
static uint8_t fkv_get_kvir_record(fkv_context_t ctxt, 
                                   uint32_t* record_page_num, 
                                   fkv_record_header_t *record_header, 
                                   uint32_t dither_low_limit_page, 
                                   uint32_t dither_high_limit_page)
{
    int8_t valid_record_found = 0;
    int8_t low_limit_found = 0;
    uint32_t dither = 0;
    //uint8_t buffer[FKV_BLOCK_SIZE];
    //use ctxt->page_buffer to avoid growing the stack too much
    uint8_t *buffer = ctxt->page_buffer;
    static const uint32_t total_dither_count = ((65536 / FKV_PAGESIZE) + 1); //65536 is the value size    
      
    while (dither < total_dither_count)
    {                  
        // Check Forward or the given page
        uint32_t forward_page_num = (*record_page_num + dither);
        if (forward_page_num <= dither_high_limit_page) 
        {
            fkv_hal_read((uint32_t *)buffer, FKV_BLOCK_SIZE
                    , (ctxt->base_page + (forward_page_num  % ctxt->size_in_pages)) * FKV_PAGESIZE);
            //Check valid page
            valid_record_found = fkv_valid_record(ctxt, (fkv_record_header_t *)buffer);
            if (valid_record_found)
            {
                *record_page_num = forward_page_num;
                *record_header = *(fkv_record_header_t *)buffer;
                break;
            }
            else
            {
                //fprintf(stderr, "Invalid %u+%u 0x%08x %u %u %u", forward_page_num, dither, record_header->signature
                //                , record_header->commit_record.log_start_page, record_header->commit_record.next_node
                //                , record_header->key_size);
            }
        }

        // Check backwards if we are checking beyond the given page
        if (dither > 0)
        {
            uint32_t back_page_num = (*record_page_num < dither)
                                   ? 0u // a consequence of adding ctxt->size_in_pages to keep low < high
                                        // is that we should never need to wrap when subtracting
                                   : (*record_page_num - dither);
            if (back_page_num >= dither_low_limit_page) 
            {     
                fkv_hal_read((uint32_t *)buffer, FKV_BLOCK_SIZE
                        , (ctxt->base_page + (back_page_num % ctxt->size_in_pages)) * FKV_PAGESIZE);
                // Check valid page
                valid_record_found = fkv_valid_record(ctxt, (fkv_record_header_t *)buffer);
                if (valid_record_found)
                {
                    *record_page_num = back_page_num;                
                    *record_header = *(fkv_record_header_t *)buffer;

                    if (back_page_num != dither_low_limit_page)
                    {
                        break;
                    }
                    else
                    {
                        // we'd prefer to find a record other than dither_low_limit_page if it exists
                        low_limit_found = 1;
                    }
                }
            }
        }
        dither++;
    }
    
    return valid_record_found || low_limit_found;
}

static bool fkv_confirm_continuation_header (fkv_context_t ctxt, void *node, uint64_t sequence_number)
{
    fkv_record_header_t *hdr = (fkv_record_header_t *)node; // this node contains a header

    return (hdr->flags == 0u)
        && (hdr->key_size == 0u)
        && (hdr->val_size == 0u)
        && (hdr->signature == 0u)
        && (hdr->commit_record.sequence_number == sequence_number)
        ;
}

int8_t fkv_commit_recovery (fkv_context_t ctxt)
{
    fkv_record_header_t low_record_header;
    fkv_record_header_t mid_record_header;
    fkv_record_header_t high_record_header;
    uint32_t low_page = (ctxt->size_in_pages / 4);
    uint32_t mid_page;
    uint32_t high_page = ((ctxt->size_in_pages * 3) / 4);
        
    int8_t low_found = fkv_get_kvir_record(ctxt, &low_page, &low_record_header, 0, high_page);
#ifdef FKV_DEBUG_RECOVERY
    if (0u != verbose_storage_debug)
    {
        fprintf(stderr, "Get %u [%u %u] returned %d %u (%llu)\n", low_page, 0, high_page, low_found, low_page, low_record_header.commit_record.sequence_number);
    }
#endif
    int8_t high_found = fkv_get_kvir_record(ctxt, &high_page, &high_record_header, low_page, ctxt->size_in_pages);
#ifdef FKV_DEBUG_RECOVERY
    if (0u != verbose_storage_debug)
    {
        fprintf(stderr, "Get %u [%u %u] returned %d %u (%llu)\n", high_page, low_page, 0, high_found, high_page, high_record_header.commit_record.sequence_number);
    }
#endif
    
    if ((low_found == 0) && (high_found == 0))
    {   
        high_page = low_page;
        high_record_header = low_record_header;     
        low_page = 0;
        if (fkv_get_kvir_record(ctxt, &low_page, &low_record_header, 0, high_page) == 0)
        {
#ifdef FKV_DEBUG_RECOVERY
            if (0u != verbose_storage_debug)
            {
                fprintf(stderr, "Get0 %u [%u %u] returned %d %u\n", low_page, 0, high_page, 0, low_page);
            }
#endif
            // There was no record @ page zero and our first probe failed
            // to find low and high. Therefore either there is no data,
            // a corrupted partition, or a erase size that is greater 
            // than 50% of the partition.
            return -1;
        }        
#ifdef FKV_DEBUG_RECOVERY
        if (0u != verbose_storage_debug)
        {
            fprintf(stderr, "Get0 %u [%u %u] returned %d %u\n", low_page, 0, high_page, 1, low_page);
        }
#endif
    }    
    else if ((low_found == 1) && (high_found == 0))
    {
        // high_page = high_page;
        // high_record_header = high_record_header;
        // low_page = low_page;
        // low_record_header = low_record_header;
    }
    else if ((low_found == 0) && (high_found == 1))
    {
        // swap
        mid_page = high_page;
        mid_record_header = high_record_header;
        high_page = low_page + ctxt->size_in_pages;
        high_record_header = low_record_header;
        low_page = mid_page;
        low_record_header = mid_record_header;
    }
    else // ((low_found == 1) && (b_found == 1))
    {
        if (high_record_header.commit_record.sequence_number < low_record_header.commit_record.sequence_number)
        {
            // high_page = high_page;
            // high_record_header = high_record_header;
            // low_page = low_page;
            // low_record_header = low_record_header;
        }
        else if (high_record_header.commit_record.sequence_number > low_record_header.commit_record.sequence_number)
        {
            // swap
            mid_page = high_page;
            mid_record_header = high_record_header;
            high_page = low_page + ctxt->size_in_pages;
            high_record_header = low_record_header;
            low_page = mid_page;
            low_record_header = mid_record_header;
        }
        else
        {
            // same sequence number!!!???
        }
    }

    do
    {    
        uint32_t candidate_page = mid_page = low_page + ((high_page - low_page) / 2u);
#ifdef FKV_DEBUG_RECOVERY
        if (0u != verbose_storage_debug)
        {
            fprintf(stderr, "Get %u [%u %u]", mid_page, low_page, high_page);
        }
#endif
        int8_t mid_found = fkv_get_kvir_record(ctxt, &mid_page, &mid_record_header, low_page, high_page);
#ifdef FKV_DEBUG_RECOVERY
        if (0u != verbose_storage_debug)
        {
            fprintf(stderr, "returned %d %u %llu\n", mid_found, mid_page, mid_record_header.commit_record.sequence_number);
        }
#endif
        if (mid_found) 
        {
            if (low_page == mid_page)
            {
                // FOUND IT!
#ifdef FKV_DEBUG_RECOVERY
                if (0u != verbose_storage_debug)
                {
                    fprintf(stderr, "Found Page %u\n", mid_page);
                }
#endif
                break;
            }
            else
            {
                if (mid_record_header.commit_record.sequence_number < low_record_header.commit_record.sequence_number)
                {
                    if (high_page == mid_page)
                    {
                        // We would make no progress by setting high_page to the same value again
                        // However, if low_page < candidate_page < high_page, keep trying
                        //
                        if ((low_page < candidate_page) && (candidate_page < high_page))
                        {
                            high_page = candidate_page;
                            // high_record_header = mid_record_header; -- unused
                        }
                        else
                        {
                            // FOUND IT!
                            fprintf(stderr, "Found Page the Hard Way %u not %u\n", low_page, mid_page);
#ifdef FKV_DEBUG_RECOVERY
                            if (0u != verbose_storage_debug)
                            {
                                fprintf(stderr, "Low Hdr: %u %u 0x%08x (%llu)\n"
                                        , low_record_header.key_size, low_record_header.val_size, low_record_header.signature
                                        , low_record_header.commit_record.sequence_number);
                                fprintf(stderr, "Mid Hdr: %u %u 0x%08x (%llu)\n"
                                        , mid_record_header.key_size, mid_record_header.val_size, mid_record_header.signature
                                        , mid_record_header.commit_record.sequence_number);
                            }
#endif
                            break;
                        }
                    }
                    else
                    {
                        high_page = mid_page;
                        // high_record_header = mid_record_header; -- unused
                    }
                }
                else if (mid_record_header.commit_record.sequence_number > low_record_header.commit_record.sequence_number) 
                {                    
                    low_page = mid_page;
                    low_record_header = mid_record_header;
                }
                else
                {
                    // same sequence number!!!???
                            // FOUND IT!?
                            fprintf(stderr, "Found Page the Impossible Way %u not %u\n", low_page, mid_page);
#ifdef FKV_DEBUG_RECOVERY
                            if (0u != verbose_storage_debug)
                            {
                                fprintf(stderr, "Low Hdr: %u %u 0x%08x (%llu)\n"
                                        , low_record_header.key_size, low_record_header.val_size, low_record_header.signature
                                        , low_record_header.commit_record.sequence_number);
                                fprintf(stderr, "Mid Hdr: %u %u 0x%08x (%llu)\n"
                                        , mid_record_header.key_size, mid_record_header.val_size, mid_record_header.signature
                                        , mid_record_header.commit_record.sequence_number);
                            }
#endif
                            break;
                }            
            }
        }
        else
        {
            //No mid page found, mid page becomes new high
            high_page = mid_page;
            // high_record_header = mid_record_header; -- unused
        }
    } 
    while (1);

    low_page %= ctxt->size_in_pages; // restore canonical page numbering to avoid overflow in calculations below

    // get next_node by scanning forward in record
    uint32_t treenodes_offset_bytes
     = low_record_header.key_size + low_record_header.val_size + sizeof(fkv_record_header_t);
    fkv_noderef_t treenodes_offset = (treenodes_offset_bytes + sizeof(fkv_node_t) - 1u) / sizeof(fkv_node_t);

    fkv_noderef_t next_node = ((low_page * FKV_NODES_PER_PAGE) + treenodes_offset)
                                 % (ctxt->size_in_pages * FKV_NODES_PER_PAGE);

    if (fkv_node_is_at_page_boundary(ctxt, next_node))
    {
        if (fkv_node_is_at_storage_end(ctxt, next_node))
        {
            // wrap to the beginning of the storage
            next_node = 0u; // note that zero is FKV_NULL_NODE but we skip that slot
        }
        else
        {
            // no need to wrap
        }
        // TODO - we could check that the node is a continuation header, i.e., all zeros, except 
        //        sequence number, which should match low_record_header.commit_record.sequence_number
        //        See fkv_confirm_continuation_header()
        // But, it will be a continuation header if the partition is OK; what would we do if it isn't? 
        //
        next_node += 1u;
    }
    else
    {
        // ok
    }

    // We expect that an extra memset() will be offset by the use of compiler optimized memcmp
    fkv_node_t sentinal_node;
    memset(&sentinal_node, 0xffu, sizeof(fkv_node_t));

    int nodes_checked = 0;

    while (1)
    {
        fkv_node_t candidate_node;
        fkv_read_node(ctxt, &candidate_node, next_node);
        // if node is all 1s, break
        if (0 == memcmp(&candidate_node, &sentinal_node, sizeof(fkv_node_t))) break;
        // don't run forever
        if (++nodes_checked > 1000) return -2;
        // increment next_node
        if (fkv_node_is_at_page_boundary(ctxt, next_node))
        {
            fkv_noderef_t saved_ref = next_node; // There is an edge case: when we are actually at
            // the end of  the partition, and at the end of a record, the recovered noderef should
            // be at the end of the partition, not the start (where it is for an empty partition).
            // But, we need next_node to be zero for fkv_read_node() below to see if we need to 
            // continue searching.

            if (fkv_node_is_at_storage_end(ctxt, next_node))
            {
                // wrap to the beginning of the storage
                next_node = 0u; // note that zero is FKV_NULL_NODE but we skip that slot
            }
            else
            {
                // no need to wrap
            }

            fkv_read_node(ctxt, &candidate_node, next_node);

            if (fkv_confirm_continuation_header(ctxt, &candidate_node, low_record_header.commit_record.sequence_number))
            {
                next_node += 1u; // skip the continuation header
            }
            else
            {
                if (0u == next_node) { next_node = saved_ref; } else { /* all set */ }
                break; // we ended at the page boundary, we're done
            }
        }
        next_node += 1u;
    }

    // update commit record from header and recovered next_node
    ctxt->commit_record.log_start_page  = low_record_header.commit_record.log_start_page;
    ctxt->commit_record.next_node       = next_node;
    ctxt->commit_record.sequence_number = low_record_header.commit_record.sequence_number;
    // TODO -- actually commit? Maybe not. The next transaction will do it.
    return 0;
}

#endif    

static uint32_t fkv_gc_expunge (fkv_context_t ctxt, uint16_t key_size, uint16_t value_size)
{
    uint32_t pages = (sizeof(fkv_record_header_t) + key_size + value_size + FKV_PAGESIZE - 1u) / FKV_PAGESIZE;
    fkv_noderef_t noderef = ((ctxt->commit_record.log_start_page + pages) % ctxt->size_in_pages) * FKV_NODES_PER_PAGE;
    // reclaim the space by incrementing ctxt->log_start_page (below)
    // reclaim any following Index pages

    fkv_record_op(5u);

    if (0u != verbose_storage_debug)
    {
        fprintf(stderr, "fkv_gc_expunge k %u v %u\n", key_size, value_size);
        fprintf(stderr, "fkv_gc_expunge n %u p %u\n", noderef, pages);
    }

    do
    {
        fkv_read_page_buffer(ctxt, noderef);

        uint16_t flags      = ((fkv_record_header_t *)(ctxt->page_buffer))->flags;
        uint16_t key_size   = ((fkv_record_header_t *)(ctxt->page_buffer))->key_size;
        uint16_t value_size = ((fkv_record_header_t *)(ctxt->page_buffer))->val_size;

        if ((flags | key_size | value_size) == 0u)
        {
            pages += 1;
            noderef = fkv_increment_noderef(ctxt, noderef, FKV_NODES_PER_PAGE);
        }
        else
        {
            break; // terminate the loop
        }
    }
    while (1);

    if (ctxt->commit_record.log_start_page >= (ctxt->size_in_pages - pages))
    {
        ctxt->gc_wraparounds += 1u;
        ctxt->commit_record.log_start_page = pages - (ctxt->size_in_pages - ctxt->commit_record.log_start_page);  // reclaim the space
    }
    else
    {
        ctxt->commit_record.log_start_page += pages;  // reclaim the space
    }
    ctxt->gc_records_expunged += 1u;

    return pages;
}

static void fkv_gc_forward (fkv_context_t ctxt, uint16_t key_size, uint16_t value_size, uint8_t *treekey)
{
    fkv_noderef_t from_node = ctxt->commit_record.log_start_page * FKV_NODES_PER_PAGE;
    fkv_noderef_t next_node = ctxt->commit_record.next_node;
    uint32_t pages = (sizeof(fkv_record_header_t) + key_size + value_size + FKV_PAGESIZE - 1u) / FKV_PAGESIZE;
    uint32_t bytes = (sizeof(fkv_record_header_t) + key_size + value_size                    ) % FKV_PAGESIZE;
    fkv_noderef_t valref = FKV_NULL_NODEREF;

    fkv_record_op(6u);

    if (0u != verbose_storage_debug)
    {
        fprintf(stderr, "fkv_gc_forward k %u v %u\n", key_size, value_size);
        fprintf(stderr, "fkv_gc_forward f %u n %u p %u b %u\n", from_node, next_node, pages, bytes);
    }

    // The page in the page buffer is the header page of this record; we need to update the commit
    // record for the recovery code.
#ifdef FKV_OPTION_REMOVEABLE_RECOVERABLE
    ctxt->commit_record.sequence_number++;
    ((fkv_record_header_t *)(ctxt->page_buffer))->commit_record = ctxt->commit_record;    
    //((fkv_record_header_t *)(ctxt->page_buffer))->signature = FKV_RECORD_SIGNATURE;  -- redundant
#endif    

    // top up next_node if it's not at the start of a page
    //
    if (fkv_node_is_at_page_boundary(ctxt, next_node))
    {
        // all set
    }
    else
    {
        next_node = (fkv_page_number_of_page_buffer(ctxt) + 1u) * FKV_NODES_PER_PAGE;
    }
    if (fkv_node_is_at_storage_end(ctxt, next_node))
    {
        // wrap to the beginning of the storage
        next_node = 0u;
    }
    else
    {
        // no need to wrap
    }
    valref = next_node; // this is where we're putting the record!

    next_node += 1u; // this makes fkv_page_number_of_page_buffer() and fkv_hal_write_page_buffer() work correctly
    ctxt->commit_record.next_node = next_node;

    if (0u != verbose_storage_debug)
    {
        fprintf(stderr, "fkv_gc_forward f %u n %u p %u b %u\n", from_node, next_node, pages, bytes);
    }

    // copy pages, updating next_node
    // leave the last one in the buffer dirty
    // this loop starts and ends with a page in the page buffer
    while ((pages > 1u) || ((pages == 1u) && (bytes == 0)))
    {
        fkv_write_page_buffer(ctxt);
        next_node = fkv_increment_noderef(ctxt, next_node, FKV_NODES_PER_PAGE);
        ctxt->commit_record.next_node = next_node;
        from_node = fkv_increment_noderef(ctxt, from_node, FKV_NODES_PER_PAGE);
        if (pages > 1u) fkv_read_page_buffer(ctxt, from_node);
        pages -= 1u;
    }
    // adjust next_node by rounding up to beyond data
    //
    next_node += ((bytes + (sizeof(fkv_node_t) - 1u)) / sizeof(fkv_node_t)) - 1u; // -1 to compensate for + 1u above
    ctxt->commit_record.next_node = next_node;
    ctxt->page_buffer_is_dirty = (bytes != 0);

#if defined(FKV_OPTION_REMOVEABLE_RECOVERABLE)
    if (bytes < sizeof(fkv_record_header_t))
    {
        memset(&ctxt->page_buffer[bytes], 0u, sizeof(fkv_record_header_t) - bytes);
    }
#endif

    if (0u != verbose_storage_debug)
    {
        fprintf(stderr, "fkv_gc_forward f %u n %u p %u b %u\n", from_node, next_node, pages, bytes);
    }

    // insert the kv
    ctxt->tree_root = fkv_insert(ctxt, ctxt->tree_root, treekey, valref);

#if defined(FKV_OPTION_REMOVEABLE_RECOVERABLE)
    fkv_page_fill(ctxt);
#endif

    fkv_commit(ctxt);

    fkv_gc_expunge(ctxt, key_size, value_size);
    ctxt->gc_records_expunged  -= 1u;
    ctxt->gc_records_forwarded += 1u;
}

uint32_t fkv_gc_one_record (fkv_context_t ctxt)  /* returns number of pages reclaimed (may be zero) */
{
    fkv_record_op(7u);

    if (ctxt->page_buffer_is_dirty)
    {
        fkv_write_page_buffer(ctxt);
    }
    else
    {
        // don't bother writing clean buffer
    }

    uint32_t res = 0;
    fkv_noderef_t noderef = ctxt->commit_record.log_start_page * FKV_NODES_PER_PAGE;

    fkv_read_page_buffer(ctxt, noderef);

    uint16_t flags      = ((fkv_record_header_t *)(ctxt->page_buffer))->flags;
    uint16_t key_size   = ((fkv_record_header_t *)(ctxt->page_buffer))->key_size;
    uint16_t value_size = ((fkv_record_header_t *)(ctxt->page_buffer))->val_size;

    
    if ((flags | key_size | value_size) == 0u)
    {
        // this is an index page; reclaim it
        // this should have been done by the last fkv_gc_expunge, but belt & suspenders
        if (0u != verbose_storage_debug)
            printf("fkv_gc_one_record index expunging %u %u %llu\n"
                , noderef, noderef / FKV_NODES_PER_PAGE, ((uint64_t *)ctxt->page_buffer)[4]);
        res = fkv_gc_expunge(ctxt, FKV_MAX_KEY_SIZE, 0u);
    }
    else
    {
        uint8_t treekey[FKV_TREE_KEY_SIZE];
        fkv_process_key(treekey, &ctxt->page_buffer[sizeof(fkv_record_header_t)], key_size, flags);

        fkv_noderef_t treenode = fkv_lookup(ctxt, ctxt->tree_root, treekey, FKV_NONESUCH);

        if (0u != verbose_storage_debug)
        {
             printf("fkv_gc_one_record keysize: %u %d pk %02x%02x%02x%02x%02x%02x%02x%02x k %08x%08x v %016llx\n"
                , key_size, memcmp(&ctxt->page_buffer[sizeof(fkv_record_header_t)], treekey, FKV_TREE_KEY_SIZE)
                , treekey[0], treekey[1], treekey[2], treekey[3], treekey[4], treekey[5], treekey[6], treekey[7]
                , ((uint32_t *)ctxt->page_buffer)[2], ((uint32_t *)ctxt->page_buffer)[1]
                , ((uint64_t *)ctxt->page_buffer)[4]);
        }
        
        if ((treenode == FKV_NONESUCH) || (treenode != noderef))
        {
            // this record is not reachable from Index, so it's garbage
            if (0u != verbose_storage_debug)
                printf("fkv_gc_one_record expunging %u %u %u %016llx\n"
                    , noderef, noderef / FKV_NODES_PER_PAGE, treenode, ((uint64_t *)ctxt->page_buffer)[4]);
                            
            res = fkv_gc_expunge(ctxt, key_size, value_size);
        }
        else if ((flags & FKV_FLAG_EXPUNGEABLE) == FKV_FLAG_EXPUNGEABLE)
        {
            if (0u != verbose_storage_debug)
                printf("fkv_gc_one_record auto expunging %u %u %llu\n"
                    , noderef, noderef / FKV_NODES_PER_PAGE, ((uint64_t *)ctxt->page_buffer)[4]);
                                     
            fkv_allocate_kvir(ctxt, NULL, 0, 0, NULL, 0);
            ctxt->tree_root = fkv_delete(ctxt, ctxt->tree_root, treekey);

#if defined(FKV_OPTION_REMOVEABLE_RECOVERABLE)
            fkv_page_fill(ctxt);
#endif
            if (ctxt->page_buffer_is_dirty)
            {
                fkv_write_page_buffer(ctxt);
            }
            else
            {
                // don't bother writing clean buffer
            }
            res = fkv_gc_expunge(ctxt, key_size, value_size);            
                        
        }
        else
        {            
            if (0u != verbose_storage_debug)
                printf("fkv_gc_one_record forwarding %u %u %u %llu -> %u\n"
                    , noderef, noderef / FKV_NODES_PER_PAGE, treenode, ((uint64_t *)ctxt->page_buffer)[4], ctxt->commit_record.next_node);
            fkv_gc_forward(ctxt, key_size, value_size, treekey);
            res = 0u;
        }
    }
    return res;
}

void fkv_process_key (uint8_t *treekey, uint8_t *key, uint16_t key_size, uint16_t flags)
{
    if ((flags & FKV_FLAG_SHA_1_KEY) == FKV_FLAG_SHA_1_KEY)
    {
        // treekey is SHA1 hash of key
        // assert FKV_TREE_KEY_SIZE == 20 == size of SHA1 hash (160 bits)
#if (FKV_TREE_KEY_SIZE != SHA1HashSize)
#error "Invalid FKV_TREE_KEY_SIZE does not match SHA1_DIGEST_SIZE"
#endif      
        SHA1Context sha1_cntx;
        int ret_code = 0;
        ret_code = SHA1Reset(&sha1_cntx);
        if(ret_code !=  shaSuccess)
        {
          fprintf(stderr, "SHA1Reset() failed with return %d\n", ret_code);
          fkv_panic();
        }
        
        ret_code = SHA1Input(&sha1_cntx, key, key_size);
        if(ret_code !=  shaSuccess)
        {
          fprintf(stderr, "SHA1Input() failed with return %d\n", ret_code);
          fkv_panic();
        }
        
        ret_code = SHA1Result(&sha1_cntx, treekey);        
        if(ret_code !=  shaSuccess)
        {
          fprintf(stderr, "SHA1Result() failed with return %d\n", ret_code);
          fkv_panic();
        }
    }
    else
    {
        // the default is FKV_FLAG_PREFIX_KEY
        // treekey is first FKV_TREE_KEY_SIZE bytes of key, zero padded if necessary
        if (key_size < FKV_TREE_KEY_SIZE)
        {
            memcpy(treekey, key, key_size);
            memset(&treekey[key_size], 0, FKV_TREE_KEY_SIZE - key_size);
        }
        else
        {
            memcpy(treekey, key, FKV_TREE_KEY_SIZE);
        }
    }
}

/* */
