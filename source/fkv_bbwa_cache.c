/** @file fkv_bbwa_cache.c
*
* @brief This module implements the cache for SLI FKV Key Value storage manager.
*
* @par
* Copyright © 2016 Sunrise Labs, Inc.
* All rights reserved.
*/

#include <stdio.h>

#include "fkv_bbwa_cache.h"
#include "fkv_storage.h"
#include "fkv_hal.h"

uint8_t verbose_cache_debug = 0u;

#ifdef FKV_USE_MURMUR3

/* Murmur3 hash function */

// Murmur3 magic numbers for 32-bit hashing
#define c1  (0xcc9e2d51)
#define c2  (0x1b873593)

#define sd  (0x98765432)

static inline uint32_t rot32 (uint32_t val, int shift)
{
    // note that shifting by 32 is undefined, so don't do it
    return shift == 0 ? val : ((val >> shift) | (val << (32 - shift)));
}

// Murmur3 32-bit to 32-bit integer hash
static inline uint32_t fmix (uint32_t h)
{
    h ^= h >> 16;
    h *= 0x85ebca6b;
    h ^= h >> 13;
    h *= 0xc2b2ae35;
    h ^= h >> 16;
    return h;
}

// Murmur3 step
static inline uint32_t Mur (uint32_t a, uint32_t h)
{
    // Helper from Murmur3 for combining two 32-bit values.
    a *= c1;
    a = rot32(a, 17);
    a *= c2;
    h ^= a;
    h = rot32(h, 19);
    return h * 5 + 0xe6546b64;
}

static inline uint32_t hash_noderef (fkv_noderef_t nr)
{
    union { uint32_t u; signed char v[4]; } hu;
    uint32_t b = sd; // seed
    uint32_t c = 9;
    hu.u = nr;
    b = b * c1 + hu.v[0]; // assumes little endian
    c ^= b;
    b = b * c1 + hu.v[1];
    c ^= b;
    b = b * c1 + hu.v[2];
    c ^= b;
    return fmix(Mur(b, Mur(3, c)));
}

#endif

#ifdef FKV_USE_JENKINS

// Jenkins' lookup3 optimized for a 3-byte input

#define rot(x,k) (((x)<<(k)) | ((x)>>(32-(k))))

static inline uint32_t hash_noderef (fkv_noderef_t nr)
{
    uint32_t a,b,c;
    a = b = c = 0xdeadbefb;
    a += nr;
    c ^= b; c -= rot(b,14);
    a ^= c; a -= rot(c,11);
    b ^= a; b -= rot(a,25);
    c ^= b; c -= rot(b,16);
    a ^= c; a -= rot(c,4); 
    b ^= a; b -= rot(a,14);
    c ^= b; c -= rot(b,24);
    return c;
}

#endif

#ifdef FKV_USE_FNV1A

/*
fnv1a
hash = offset_basis
for each octet_of_data to be hashed
        hash = hash xor octet_of_data
        hash = hash * FNV_prime
return hash

32-bit constants:
  16777619 -- FNV_prime
2166136261 -- offset basis
*/

static inline uint32_t hash_noderef (fkv_noderef_t nr)
{
    uint32_t hash = 2166136261;   // offset basis
    hash = hash ^ (nr & 0xFFu);
    hash = hash * 16777619;     // FNV_prime
    hash = hash ^ ((nr >> 8) & 0xFFu);
    hash = hash * 16777619;     // FNV_prime
    hash = hash ^ ((nr >> 16) & 0xFFu);
    hash = hash * 16777619;     // FNV_prime
    return hash;
}

#endif

/* the cache */

/* caller of fkv_delete_bucket ensures that bucket's cache node is not locked
** caller of fkv_delete_bucket uses bucket's cache node immediately or adds it to free list
*/
static inline void fkv_delete_bucket (fkv_context_t ctxt, uint32_t probe)
{
    bool saw_locked_node = false;

    uint32_t hindx = probe; // record the hash index of the bucket to be deleted

    /* We can't just empty the slot; that would affect searches for other keys that have a hash value earlier than 
    ** the emptied slot, but that are stored in a position later than the emptied slot.
    ** Instead, it is necessary to search forward through the following slots of the table until finding either 
    ** another empty slot or a key that can be moved to slot hindx (that is, a key whose hash value is equal to or 
    ** earlier than hindx). When an empty slot is found, then emptying slot hindx is safe and the deletion process 
    ** terminates. When the search finds a key that can be moved to slot hindx, it performs this move. This has the 
    ** effect of speeding up later searches for the moved key, but it also empties out another slot, later in the 
    ** same block of occupied slots. The search for a movable key continues for the new emptied slot, in the same 
    ** way, until it terminates by reaching a slot that was already empty. In this process of moving keys to earlier 
    ** slots, each key is examined only once. Therefore, the time to complete the whole process is proportional to 
    ** the length of the block of occupied slots containing the deleted key, matching the running time of the other 
    ** hash table operations.
    ** Note that if the search finds a locked key that could otherwise be moved to the emptied slot, it can't be 
    ** moved because it's locked, and so the emptied slot cannot be marked NULL. So, if we can't find another key to 
    ** moved into the emptied slot, then we must resort to using FKV_DELETED_NODEREF.
    */

    while (1)
    {
        probe = (probe + 1) % FKV_HASHTSIZE;
        if (probe == hindx)
        {
            // table full!
            ctxt->fkv_hash_table[hindx].noderef = FKV_DELETED_NODEREF;
            break;
        }
        if (ctxt->fkv_hash_table[probe].noderef == FKV_NULL_NODEREF)
        {
            // bingo!
            ctxt->fkv_hash_table[hindx].noderef = (saw_locked_node) ? FKV_DELETED_NODEREF : FKV_NULL_NODEREF;
            break;
        }
        if (ctxt->fkv_hash_table[probe].noderef == FKV_DELETED_NODEREF)
        {
            // skip the deleted flag
            continue;
        }
        uint32_t pindx = hash_noderef(ctxt->fkv_hash_table[probe].noderef) % FKV_HASHTSIZE;
        if (fkv_cache_chain_length(pindx, probe) >= fkv_cache_chain_length(hindx, probe))
        {
            // slot probe is a candidate to move to hindx
            if (cache_entry_is_locked(ctxt, ctxt->fkv_hash_table[probe].cache_slot))
            {
                saw_locked_node = true;
            }
            else
            {
                // move it 
                ctxt->fkv_hash_table[hindx] = ctxt->fkv_hash_table[probe];
                // and start again from here
                hindx = probe;
                saw_locked_node = false;
            }
        }
    }
}

static void fkv_evict (fkv_context_t ctxt)
{
    uint16_t eindx  = ctxt->eviction_indx;
    uint16_t smslot = fkv_get_free_cache_slot(ctxt); // expected to return FKV_CACHESIZE, i.e., empty list
    uint16_t smbuck = FKV_HASHTSIZE;
    uint32_t smsize = UINT32_MAX;

    if (verbose_cache_debug) printf("evict  %4u %4u\n", smslot, eindx);

    if (FKV_CACHESIZE == smslot)
    {
        // delete slot from hash table and put its cache slot on free list

        uint16_t probe = eindx;

        do
        {
            if (   (ctxt->fkv_hash_table[probe].noderef != FKV_NULL_NODEREF)
                && (ctxt->fkv_hash_table[probe].noderef != FKV_DELETED_NODEREF)
                && (!cache_entry_is_locked(ctxt, ctxt->fkv_hash_table[probe].cache_slot)) )
            {
                uint16_t cslot = ctxt->fkv_hash_table[probe].cache_slot;
                uint32_t csize = fkv_node_size(&ctxt->fkv_node_cache[cslot]);

                if (csize < smsize)
                {
                    // the best candidate, so record it
                    smbuck = probe;
                    smslot = cslot;
                    smsize = csize;
                }
                else
                {
                    // skip it, it's bigger than something we've seen already
                }
            }
            else
            {
                // skip it, it's empty or locked
            }
            probe = (probe + 1) % FKV_HASHTSIZE;

        } while ((smsize > FKV_EVICT_SIZE) && probe != eindx);

        if (smbuck == FKV_HASHTSIZE)
        {
            // error, all nodes locked or empty
            // PANIC!
            fprintf(stderr, "fkv_evict() no bucket found in hash table!\n");
            fkv_panic();
            return;
        }
        ctxt->eviction_indx = (smbuck + 1) % FKV_HASHTSIZE; // move the eviction clock hand

        // delete the hashtable bucket smbuck
        fkv_delete_bucket (ctxt, smbuck);
    }

    fkv_put_free_cache_slot(ctxt, smslot); // add slot to freelist

    if (verbose_cache_debug) printf("evict   (%4u) %4u\n", eindx, smslot);
}

/* fkv_htfindins returns an index into the cache
*/
static uint32_t fkv_htfindins(fkv_context_t ctxt, fkv_noderef_t noderef)
{
    uint32_t hindx = hash_noderef(noderef) % FKV_HASHTSIZE;
    uint32_t probe = hindx;
    uint32_t hole = FKV_HASHTSIZE;
    uint32_t res  = FKV_CACHESIZE;

    if (verbose_cache_debug) printf("findins %4u\n", noderef);

    while (FKV_CACHESIZE == res)
    {
        while (ctxt->fkv_hash_table[probe].noderef != FKV_NULL_NODEREF)
        {
            if (ctxt->fkv_hash_table[probe].noderef == noderef)
            {
                //fkv_bucket_t *pbucket = &
                res = ctxt->fkv_hash_table[probe].cache_slot; // we found the node at probe
                break;
            }
            else
            {
                if (hole == FKV_HASHTSIZE && ctxt->fkv_hash_table[probe].noderef == FKV_DELETED_NODEREF)
                {
                    // record the first hole encountered
                    // this will be the place to insert the node if not found
                    hole = probe;
                }
                probe = (probe + 1) % FKV_HASHTSIZE;
                if (probe == hindx)
                {
                    // table full! cannot happen! (because FKV_HASHTSIZE > FKV_CACHESIZE)
                    // PANIC!
                    fprintf(stderr, "fkv_htfindins() hash table full!\n");
                    fkv_panic();
                    break;
                }
            }
        }
        if (FKV_CACHESIZE == res)
        {
            // not in cache; need to read from long term storage
            // hole points to the first deleted slot, otherwise use probe
            if (hole != FKV_HASHTSIZE) probe = hole;
            // get a free node, or evict if there is no free node
            res = fkv_get_free_cache_slot(ctxt);
            if (FKV_CACHESIZE == res)
            {
                fkv_evict(ctxt); // puts one free cache slot on free list, so this case only happens once
                // the evict may free a bucket earlier than probe, so restart the search
                probe = hindx;
                hole = FKV_HASHTSIZE;
            }
            else
            {
                fkv_read_node(ctxt, &ctxt->fkv_node_cache[res], noderef); 
                ctxt->fkv_hash_table[probe].noderef = noderef;
                ctxt->fkv_hash_table[probe].cache_slot = res;
            }
        }
    }
    if (verbose_cache_debug) printf("findins %4u -> (%4u) %4u\n", noderef, hindx, probe);
    return res;
}

/* fkv_cache_node_locked never returns NULL
   the node is locked, so it will not be evicted until it is unlocked
*/
fkv_node_t *fkv_cache_node_locked(fkv_context_t ctxt, fkv_noderef_t noderef)
{
    unsigned int hindx = fkv_htfindins(ctxt, noderef);
    cache_entry_lock(ctxt, hindx);
    return &ctxt->fkv_node_cache[hindx];
}

/* fkv_cache_node_unlocked never returns NULL
   the node is not locked, so it may be evicted by another fkv_cache_ call
*/
fkv_node_t *fkv_cache_node_unlocked(fkv_context_t ctxt, fkv_noderef_t noderef)
{
    unsigned int hindx = fkv_htfindins(ctxt, noderef);
    return &ctxt->fkv_node_cache[hindx];
}

/* node may be evicted from cache
*/
void fkv_unlock_node(fkv_context_t ctxt, fkv_noderef_t noderef, fkv_node_t *node)
{
    unsigned int cindx = node - &ctxt->fkv_node_cache[0];

    cache_entry_unlock(ctxt, cindx);
}

/* node will be evicted from cache
   fkv_uncache_node() does an implicit unlock
*/
void fkv_uncache_node(fkv_context_t ctxt, fkv_noderef_t noderef, fkv_node_t *node)
{
    uint16_t cindx = node - &ctxt->fkv_node_cache[0];
    uint32_t hindx = hash_noderef(noderef) % FKV_HASHTSIZE;
    uint32_t probe = hindx;
    uint32_t slot  = FKV_CACHESIZE;

    if (verbose_cache_debug) printf("uncache %4u <- %4u\n", noderef, cindx);

    cache_entry_unlock(ctxt, cindx);

    if (verbose_cache_debug) printf("delete %4u %u\n", noderef, hindx);

    // find the bucket

    while (ctxt->fkv_hash_table[probe].noderef != FKV_NULL_NODEREF)
    {
        if (ctxt->fkv_hash_table[probe].noderef == noderef)
        {
            slot = ctxt->fkv_hash_table[probe].cache_slot; // we found the node at bucket probe
            break;
        }
        else
        {
            probe = (probe + 1) % FKV_HASHTSIZE;
            if (probe == hindx)
            {
                // table full! cannot happen!
                // PANIC!
                fprintf(stderr, "fkv_delete_evictim() not found in hash table!\n");
                fkv_panic();
                break;
            }
        }
    }

    fkv_delete_bucket(ctxt, probe);

    // assert slot == cindx
    if (slot != cindx)
    {
        printf("uncache ERROR %4u != %4u\n", slot, cindx);
    }

    fkv_put_free_cache_slot(ctxt, slot); // add slot to freelist

    // TODO - We can compress the chains that contain FKV_DELETED_NODEREF after the 
    // nodes in the chain are unlocked. We can do this incrementally, but that's less likely
    // to be successful than if we do it when the cache is completely unlocked, which is true
    // between fkv transactions.

    if (verbose_cache_debug) printf("uncache %4u %4u\n", hindx, probe);
}
