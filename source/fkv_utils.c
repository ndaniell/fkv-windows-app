/** @file fkv_utils.cpp
*
* @brief This module implements utility functions.
*
* @par
* Copyright © 2016 Sunrise Labs, Inc.
* All rights reserved.
*/
#include "fkv_utils.h"
#include "fkv_hal.h"
#include "fkv_storage.h"

#include <stdio.h>
#include <stdlib.h>

#define	_MAX_SS                 512
#define MBR_Table		446		/* MBR: Partition table offset (2) */
#define	SZ_PTE			16		/* MBR: Size of a partition table entry */

#define	ST_DWORD(ptr,val)	*(uint8_t*)(ptr)=(uint8_t)(val); *((uint8_t*)(ptr)+1)=(uint8_t)((uint16_t)(val)>>8); *((uint8_t*)(ptr)+2)=(uint8_t)((uint32_t)(val)>>16); *((uint8_t*)(ptr)+3)=(uint8_t)((uint32_t)(val)>>24)
#define	ST_WORD(ptr,val)	*(uint8_t*)(ptr)=(uint8_t)(val); *((uint8_t*)(ptr)+1)=(uint8_t)((uint16_t)(val)>>8)

#define RD_DWORD(ptr)   (*(uint8_t*)(ptr) + (*((uint8_t*)(ptr)+1) << 8) + (*((uint8_t*)(ptr)+2) << 16) + (*((uint8_t*)(ptr)+3) << 24))

static uint8_t buf[FKV_BLOCK_SIZE];  

/* The szt[] is an array of four percentages (if <= 100) or 512-byte sector counts
*/
uint8_t fkv_fdisk(const uint32_t szt[])
{  
    uint16_t i;
    uint16_t n;
    uint16_t sz_cyl;
    uint16_t tot_cyl;
    uint16_t b_cyl;
    uint16_t e_cyl;
    uint16_t p_cyl;
    uint8_t s_hd;
    uint8_t e_hd;
    uint8_t *p;          
    uint32_t sz_disk;
    uint32_t sz_part;
    uint32_t s_part;    
    
    fkv_hal_open();
    
    fkv_device_info_t dev_info;
    fkv_device_info(&dev_info);
    sz_disk = dev_info.capacity_bytes / FKV_BLOCK_SIZE;
    
    /* Determine CHS in the table regardless of the drive geometry */
    for (n = 16; n < 256 && sz_disk / n / 63 > 1024; n *= 2) ;
    if (n == 256) n--;
    e_hd = n - 1;
    sz_cyl = 63 * n;
    tot_cyl = sz_disk / sz_cyl;
    
    
    /* Create partition table */
    memset(buf, 0, FKV_BLOCK_SIZE);
    p = buf + MBR_Table; 
    b_cyl = 0;
    for (i = 0; i < 4; i++, p += SZ_PTE) 
    {
        p_cyl = (szt[i] <= 100U) ? (uint32_t)tot_cyl * szt[i] / 100 : szt[i] / sz_cyl;
        if (!p_cyl) continue;
        s_part = (uint32_t)sz_cyl * b_cyl;
        sz_part = (uint32_t)sz_cyl * p_cyl;
        if (i == 0) 
        {	
            /* Exclude first track of cylinder 0 */
            s_hd = 1;
            s_part += 63; 
            sz_part -= 63;
        } 
        else 
        {
            s_hd = 0;
        }
        e_cyl = b_cyl + p_cyl - 1;
        if (e_cyl >= tot_cyl) return -1;

        // For cards over 8M, the CHS address is insufficient, so the 
        // convention is CHS = (1023, 254, 63) to indicate LBA is used
        //
        uint16_t b_cyr = b_cyl > 1023u ? 1023u : b_cyl;
        uint16_t e_cyr = e_cyl > 1023u ? 1023u : e_cyl;

        /* Set partition table */
        p[1] = s_hd;                            /* Start head */
        p[2] = (uint8_t)(((b_cyr >> 2) & 0xc0) + 1);	/* Start sector */
        p[3] = (uint8_t)b_cyr;			/* Start cylinder */
        p[4] = 0x06;			        /* System type (temporary setting) */
        p[5] = e_hd;			        /* End head */
        p[6] = (uint8_t)(((e_cyr >> 2)  & 0xc0) + 63);	/* End sector */
        p[7] = (uint8_t)e_cyr;			/* End cylinder */
        ST_DWORD(p + 8, s_part);		/* Start sector in LBA */
        ST_DWORD(p + 12, sz_part);		/* Partition size */

        /* Next partition */
        b_cyl += p_cyl;
    }
    ST_WORD(p, 0xAA55);
    
    /* Write it to the MBR */
    fkv_hal_write((uint32_t*)buf, FKV_BLOCK_SIZE, 0);    
    return 0;
}

void fkv_mkfs(const uint8_t part_number)
{
    fkv_hal_open();
    
    fkv_hal_read((uint32_t*)buf, FKV_BLOCK_SIZE, 0);  
    uint8_t* part = buf + MBR_Table + (part_number * SZ_PTE);
    part[4] = 0x06;
}

void fkv_get_part_info(const uint8_t part_number, fkv_part_info_t* part_info)
{
    fkv_hal_open();

    fkv_hal_read((uint32_t*)buf, FKV_BLOCK_SIZE, 0);  
        
    uint8_t* part = buf + MBR_Table + (part_number * SZ_PTE);
    
    uint8_t start_head = part[1];    
    uint8_t start_sector = (0x3f & part[2]);
    uint16_t start_cylinder = (0xc0 & part[2])+ part[3];
    
    part_info->part_type = part[4];
    
    uint8_t end_head = part[5];    
    uint8_t end_sector = (0x3f & part[6]);    
    uint16_t end_cylinder = ((0xc0 & (uint16_t)part[6]) << 2)+ part[7];

#ifdef FKV_DEBUG_PARTITION_TABLE
    fprintf(stderr, "Chan start sector LBA = %d\n", RD_DWORD(part + 8));
    fprintf(stderr, "Chan partition size in sectors = %d\n", RD_DWORD(part + 12));
    fprintf(stderr, "Chan start address = %d\n", 512 * RD_DWORD(part + 8));
    fprintf(stderr, "Chan partition size in bytes = %d\n", 512 * RD_DWORD(part + 12));
#endif

    // (cylinder * Nheads + head) * Nsectors + (sector − 1)
    uint64_t CHS_start_address
     = (uint64_t)512u * ((((start_cylinder * (end_head + 1)) + start_head) * end_sector) + (start_sector - 1));
    uint64_t CHS_size_bytes
     = ((uint64_t)512u * ((((end_cylinder * (end_head + 1)) + end_head) * end_sector) + end_sector))
        - CHS_start_address;

    uint64_t LBA_start_address = (uint64_t)512u * RD_DWORD(part + 8);
    uint64_t LBA_size_bytes = (uint64_t)512u * RD_DWORD(part + 12);

    if (((1023u == start_cylinder) && (254u == start_head) && (63u == start_sector))
     || ((1023u == end_cylinder) && (254u == end_head) && (63u == end_sector)))
    {
        // (1023, 254, 63) indicates LBA is used
        part_info->start_address = LBA_start_address;
        part_info->size_bytes = LBA_size_bytes;
    }
    else if ((LBA_start_address != CHS_start_address) || (LBA_size_bytes != CHS_size_bytes))
    {
        if ((0u == LBA_start_address) || (0u == LBA_size_bytes))
        {
#ifdef FKV_DEBUG_PARTITION_TABLE
            fprintf(stderr, "WARNING: Using CHS based partition info, LBA bad: %llu %llu\n"
                , LBA_start_address
                , LBA_size_bytes);
#endif
            part_info->start_address = CHS_start_address;
            part_info->size_bytes = CHS_size_bytes;
        }
        else
        {
#ifdef FKV_DEBUG_PARTITION_TABLE
            fprintf(stderr, "WARNING: Using LBA based partition info, CHS: %llu+%llu LBA: %llu+%llu\n"
                , CHS_start_address
                , CHS_size_bytes
                , LBA_start_address
                , LBA_size_bytes);
#endif
            part_info->start_address = LBA_start_address;
            part_info->size_bytes = LBA_size_bytes;
        }
    }
    else
    {
        part_info->start_address = LBA_start_address;
        part_info->size_bytes = LBA_size_bytes;
    }

}
