/** @file fkv_api.c
*
* @brief This module implements the API the SLI FKV Key Value storage manager.
*
* @par
* Copyright © 2016 Sunrise Labs, Inc.
* All rights reserved.
*/

#include <stdint.h>
#include <stdio.h>

#include "fkv_api.h"
#include "fkv_bbwa_tree.h"
#include "fkv_storage.h"
#include "fkv_hal.h"
#include "fkv_utils.h"


// TODO -- perhaps all the extern API functions should setjmp() so fkv_panic() can longjmp()

// debug 
uint8_t verbose_api_debug = 0u;

static struct fkv_context_s context;

static void fkv_align(uint32_t start_address, uint64_t size_bytes, uint32_t *aligned_base_page, uint32_t *aligned_size_pages)
{
    *aligned_base_page = (start_address + (FKV_PAGESIZE - 1u)) / FKV_PAGESIZE;
    
    size_bytes = size_bytes - ((*aligned_base_page * FKV_PAGESIZE) - start_address);

    *aligned_size_pages =  size_bytes / FKV_PAGESIZE;
}

fkv_status_t fkv_create (fkv_context_t* ctxt, unsigned int partition)
{
    *ctxt = &context;
    (void)ctxt;
    
    memset(&context, 0u, sizeof(context));
    
    init_fkv_context(&context);    // setup context
    
    fkv_hal_open();           
    
    fkv_part_info_t part_info;
    fkv_get_part_info(partition, &part_info);

    fkv_mkfs(partition);    

    fkv_align(part_info.start_address, part_info.size_bytes, &context.base_page, &context.size_in_pages);
    
    context.commit_record.log_start_page = 0u;     // the index of the oldest live FKV_PAGESIZE page
    context.commit_record.next_node = 0u;          // empty fkv storage
#ifdef FKV_OPTION_REMOVEABLE_RECOVERABLE   
    context.commit_record.sequence_number = 0ull;
#endif
    context.tree_root = FKV_NULL_NODEREF;          // empty tree    
    
    return FKV_OK;
}

fkv_status_t fkv_open (fkv_context_t* ctxt, unsigned int partition, const uint8_t recovery)
{    
    *ctxt = &context;
    (void)ctxt;
    
    memset(&context, 0u, sizeof(context));
    
    init_fkv_context(&context);    // setup context
    
    fkv_hal_open();  
    
    fkv_part_info_t part_info;
    fkv_get_part_info(partition, &part_info);
    
    fkv_align(part_info.start_address, part_info.size_bytes, &context.base_page, &context.size_in_pages);
    
    if (recovery) 
    {
        if(fkv_commit_recovery(&context) != 0)
        {
            ctxt = NULL;
            return FKV_COMMIT_RECORD_RECOVERY_ERROR;
        }
    }
    else
    {
        fkv_hal_read_commit_record(&context.commit_record);                  
    }         
    context.tree_root = context.commit_record.next_node - 1u;
    
    return FKV_OK;
}

fkv_status_t 
fkv_put (fkv_context_t ctxt,
         uint8_t *key,          /* in */
         uint16_t key_size,     /* in */
         uint16_t flags,        /* in; flags are needed so we know how to convert Key to tree key */
         uint8_t *value,        /* in */
         uint16_t value_size    /* in */
        )
{
    if (key_size > FKV_MAX_KEY_SIZE)
    {
        return FKV_API_ARG_ERROR;
    }
    if (flags > FKV_MAX_FLAGS_VALUE)
    {
        return FKV_API_ARG_ERROR;
    }
    // check if GC required
    //
    if (fkv_gc_is_advisable(ctxt))
    {
        do
        {
            uint32_t pages_reclaimed = fkv_gc_one_record(ctxt);

            // debug
            if (1u < verbose_api_debug) printf("GC relaimed %u pages.\n", pages_reclaimed);
        }
        while (fkv_gc_is_mandatory(ctxt));
    }
    else
    {
        // no GC requried yet
    }
    // put the record in the store
    //
    fkv_noderef_t valref = fkv_allocate_kvir(ctxt, key, key_size, flags, value, value_size);
    //
    // convert the key and put (key', valref) into the tree
    //
    uint8_t treekey[FKV_TREE_KEY_SIZE];
    memset(treekey, 0, FKV_TREE_KEY_SIZE);
    
    //Add empty nodes until the next page is reached for expungable records.
    //This ensures at least two pages are utilized. This we done so that when
    //gc occurs enough pages will be freeed.
    if (((flags & FKV_FLAG_EXPUNGEABLE) == FKV_FLAG_EXPUNGEABLE))
    {
        while ((ctxt->commit_record.next_node - valref) < FKV_NODES_PER_PAGE) 
        {
            (void)fkv_allocate_node (ctxt, treekey, 0u, 0u, 0u, 0u);
        }
    }
   
    fkv_process_key(treekey, key, key_size, flags);
    ctxt->tree_root = fkv_insert(ctxt, ctxt->tree_root, treekey, valref);
      
#if defined(FKV_OPTION_REMOVEABLE_RECOVERABLE)
    fkv_page_fill(ctxt);
#endif
        
    fkv_commit(ctxt);
    return FKV_OK;
}

/* fkv_get() gets the record with Key
** For value_size and keyout_size, if the value's (key's) size exceeds the value_size (keyout_size) arg, then
** the first value_size (keyout_size) bytes are copied, but the return value is the full value's (key's) size.
** Note that the keyout may differ from key depending on the flags, e.g., FKV_FLAG_PREFIX_KEY.
*/
fkv_status_t 
fkv_get (fkv_context_t ctxt,
         uint8_t  *key,         /* in */
         uint16_t  key_size,    /* in */
         uint16_t  flags,       /* in; flags are needed so we know how to convert Key to tree key */
         uint8_t  *value,       /* out; NULL pointer ok if value not wanted */
         uint16_t *value_size,  /* in/out; in: size of value buffer, zero ok; out: size of value */
         uint8_t  *keyout,      /* out; NULL pointer ok if keyout not wanted */
         uint16_t *keyout_size  /* in/out; in: size of keyout buffer, zero ok; out: size of key */
        )
{
    fkv_status_t res = FKV_OK;
    uint8_t treekey[FKV_TREE_KEY_SIZE];
    fkv_process_key(treekey, key, key_size, flags);

    fkv_noderef_t treenode = fkv_lookup(ctxt, ctxt->tree_root, treekey, FKV_NONESUCH);
    
    if (treenode == FKV_NONESUCH)
    {
        res = FKV_NOT_FOUND;
    }
    else
    {
        fkv_read_kvir(ctxt, keyout, keyout_size, value, value_size, treenode);
        res = FKV_OK;
    }
    return res;
}

/* fkv_nxt() gets the record next after Key in lexicographical order (using memcmp)
** For value_size and keyout_size, if the value's (key's) size exceeds the value_size (keyout_size) arg, then
** the first value_size (keyout_size) bytes are copied, but the return value is the full value's (key's) size.
** Note that the keyout may be used as the key argument for the subsequent call to fkv_nxt() to sequence through the 
** records in lexicographical order (using memcmp).
*/
fkv_status_t 
fkv_nxt (fkv_context_t ctxt,
         uint8_t  *key,         /* in */
         uint16_t  key_size,    /* in */
         uint16_t  flags,       /* in; flags are needed so we know how to convert Key to tree key */
         uint8_t  *value,       /* out; NULL pointer ok if value not wanted */
         uint16_t *value_size,  /* in/out; in: size of value buffer, zero ok; out: size of value */
         uint8_t  *keyout,      /* out; NULL pointer ok if keyout not wanted */
         uint16_t *keyout_size  /* in/out; in: size of keyout buffer, zero ok; out: size of key */
        )
{
    fkv_status_t res = FKV_OK;
    uint8_t treekey[FKV_TREE_KEY_SIZE];
    fkv_process_key(treekey, key, key_size, flags);

    fkv_noderef_t treenode = fkv_nafter(ctxt, ctxt->tree_root, treekey, FKV_NONESUCH);
    
    if (treenode == FKV_NONESUCH)
    {
        res = FKV_NOT_FOUND;
    }
    else
    {
        fkv_read_kvir(ctxt, keyout, keyout_size, value, value_size, treenode);
        res = FKV_OK;
    }
    return res;
}

/* fkv_nth() returns the indexth item in key order when index >= 0
** fkv_nth() returns the treesize + indexth item in key order when index < 0
** fkv_nth() returns FKV_NOT_FOUND when index exceeds the treesize
*/
fkv_status_t 
fkv_nth (fkv_context_t ctxt,
         uint32_t  index,       /* in */
         uint8_t  *value,       /* out; NULL pointer ok if value not wanted */
         uint16_t *value_size,  /* in/out; in: size of value buffer, zero ok; out: size of value */
         uint8_t  *keyout,      /* out; NULL pointer ok if keyout not wanted */
         uint16_t *keyout_size  /* in/out; in: size of keyout buffer, zero ok; out: size of key */
        )
{
    fkv_status_t res = FKV_OK;

    fkv_noderef_t treenode = fkv_indexth(ctxt, ctxt->tree_root, index, FKV_NONESUCH);
    
    if (treenode == FKV_NONESUCH)
    {
        res = FKV_NOT_FOUND;
    }
    else
    {
        fkv_read_kvir(ctxt, keyout, keyout_size, value, value_size, treenode);
        res = FKV_OK;
    }
    return res;
}

/* fkv_rem() removes the record with Key from the Index
** It does so by appending a record to the log with no value and FKV_FLAG_EXPUNGEABLE set. 
*/
fkv_status_t 
fkv_rem (fkv_context_t ctxt,
         uint8_t *key,          /* in */
         uint16_t key_size,     /* in */
         uint16_t flags         /* in; flags are needed so we know how to convert Key to tree key */
        )
{
    if (key_size > FKV_MAX_KEY_SIZE)
    {
        return FKV_API_ARG_ERROR;
    }
    if (flags > FKV_MAX_FLAGS_VALUE)
    {
        return FKV_API_ARG_ERROR;
    }
    // check if GC required
    //
    if (fkv_gc_is_advisable(ctxt))
    {
        do
        {
            uint32_t pages_reclaimed = fkv_gc_one_record(ctxt);

            // debug
            if (1u < verbose_api_debug) printf("GC relaimed %u pages.\n", pages_reclaimed);
        }
        while (fkv_gc_is_mandatory(ctxt));
    }
    else
    {
        // no GC requried yet
    }
    // put the delete record in the store
    //
    /* unused fkv_noderef_t valref = */ 
    fkv_allocate_kvir(ctxt, key, key_size, flags | FKV_FLAG_EXPUNGEABLE, NULL, 0);
    //
    // convert the key and delete (key', ?) from the tree
    //
    fkv_status_t res = FKV_OK;
    uint8_t treekey[FKV_TREE_KEY_SIZE];
    fkv_process_key(treekey, key, key_size, flags);
    ctxt->tree_root = fkv_delete(ctxt, ctxt->tree_root, treekey);
    
#if defined(FKV_OPTION_REMOVEABLE_RECOVERABLE)
    fkv_page_fill(ctxt);
#endif
    
    fkv_commit(ctxt);
    return res;
}

fkv_status_t fkv_close (fkv_context_t ctxt)
{    
    if (ctxt == &context) fkv_hal_close();
    return FKV_OK;
}

/* */
