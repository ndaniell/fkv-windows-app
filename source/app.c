#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <windows.h>
#include <time.h>

#include "fkv_api.h"

#define PARTITION_NUMBER 0
#define KEY_SIZE 13u
#define VALUE_BUFFER_SIZE (512u* 8u)
static uint8_t value_buffer[VALUE_BUFFER_SIZE];
static uint8_t key_buffer[KEY_SIZE];
static fkv_context_t cntx;

typedef struct
{
	//uint8_t pn;
	//uint8_t id;  // SENSOR_DATA
	uint8_t ecg[3];
	uint8_t ppg[12];
	union
	{
		uint8_t acc[6];
		//uint8_t ver[4];
		///uint16_t bat;
		//uint16_t temp;
		//If these variable are un commented it padds out to 22
	};
}WaveFormData; //21 bytes

typedef struct
{
	uint16_t header;
	uint8_t serial_number;
	uint8_t rtc_high_b;
	uint8_t rtc_mid_b;
	uint8_t rtc_low_b;
	//6 bytes

	WaveFormData wave_forms[24];
	//504 bytes

	uint16_t checksum;
	//2 bytes

	//Total 512 bytes
}WaveFormDataBlock;


static uint32_t swap_uint32(uint32_t val)
{
	val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF);
	return (val << 16) | (val >> 16);
}

static uint64_t swap_uint64(uint64_t val)
{
	val = ((val << 8) & 0xFF00FF00FF00FF00ULL) | ((val >> 8) & 0x00FF00FF00FF00FFULL);
	val = ((val << 16) & 0xFFFF0000FFFF0000ULL) | ((val >> 16) & 0x0000FFFF0000FFFFULL);
	return (val << 32) | (val >> 32);
}

#define MAX_FILE_NAME_LEN 34
#define MAX_OUTPUT_PATH 255
#define FILE_BUFFER_SIZE MAX_OUTPUT_PATH + MAX_FILE_NAME_LEN

static char status_file_name[FILE_BUFFER_SIZE];
static char data_file_name[FILE_BUFFER_SIZE];

static char output_path[MAX_OUTPUT_PATH];

extern WCHAR logical_drive[50];

typedef enum
{
	Status_Record = 1,
	Data_Record = 2
	//MAX = 255
} record_type_t;

static void printHelp(void)
{
	printf("Coin-FKV-App.exe v1.0.0\n");
	printf("Converts a Coin FKV partition into 5 minute time stamped binary files\n");	
	printf("usage: Coin-FKV-App.exe drive_letter output_directory\n");
	printf("	driver_letter - The drive letter of SD card driver(e.g. F, G, H)\n");
	printf("	output_directory - The directory where to create the place the out files(e.g. C:\\Temp)\n");

}

static BOOL DirectoryExists(LPWSTR szPath)
{
	DWORD dwAttrib = GetFileAttributes(szPath);
	return (dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

int main(int argc, wchar_t **argv)
{
	if ((argc > 3) || (argc < 2))
	{
		printf("Incorrect number of command line arguments\n");
		return -1;
	}
	for (int arg_index = 0; arg_index < argc; arg_index++)
	{
		if (arg_index == 1)
		{
			wsprintf(logical_drive, L"\\\\.\\%c:", argv[arg_index][0]);
			HANDLE temp_fh = CreateFile(logical_drive,
										GENERIC_READ | GENERIC_WRITE,
										FILE_SHARE_READ | FILE_SHARE_WRITE,
										NULL,
										OPEN_EXISTING,
										FILE_ATTRIBUTE_NORMAL,
										NULL);
			if (temp_fh == INVALID_HANDLE_VALUE)
			{				
				printf("Invalid Drive Letter(%d)\n", GetLastError());
				return -1;
			}
			if (temp_fh != NULL)
			{
				CloseHandle(temp_fh);
			}
			printf("Using Drive %c:\\\n", argv[arg_index][0]);
			
		}
		else if (arg_index == 2)
		{
			if (DirectoryExists(argv[arg_index]))
			{
				wcstombs(output_path, argv[arg_index], MAX_OUTPUT_PATH);				
			}
			else
			{
				printf("Directory not specified or does not exist, using current working directory\n");
				memset(output_path, 0, MAX_OUTPUT_PATH);
			}
		}
		else
		{
		}
	}

    uint16_t key_size = KEY_SIZE;   
    uint16_t value_size = VALUE_BUFFER_SIZE;         

    fkv_status_t open_res = fkv_open(&cntx, PARTITION_NUMBER, 1);
    if(open_res != FKV_OK)
    {
        printf("Error Opening FKV\n"); 
        return -1;
    }

    memset(key_buffer, 0, 13);

	FILE* status_fp = NULL;
	FILE* data_fp = NULL;

	time_t status_start_time = 0;
	time_t data_start_time = 0;
    
    while (1)
    {
            fkv_status_t nxt_res = fkv_nxt(cntx,
                                           key_buffer, 
                                           KEY_SIZE, 
                                           FKV_FLAG_PREFIX_KEY | FKV_FLAG_EXPUNGEABLE,
                                           value_buffer, 
                                           &value_size,
                                           key_buffer,
                                           &key_size);
            if(nxt_res != FKV_OK)
            {
                printf("Error fkv_nxt() return %d for key ", nxt_res); 
				for (int key_index = 0; key_index < key_size; key_index++)
				{
					printf("%x", key_buffer[key_index]);
				}
				printf("\n");
                break;
            }
            else
            {   
				int8_t key_type = key_buffer[0];
				time_t time_stamp = swap_uint32(*(uint32_t*)(key_buffer + 1));
				uint64_t sequence_num = swap_uint64(*(uint64_t*)(key_buffer + 5));
				uint16_t device_id = 0;
				uint16_t pid = 0;

				if (key_type == Status_Record)
				{
					//If the start time is set and is greater than the current time stamp
					//by 5 minutes close the file so a new file can be started
					if ((status_start_time != 0) &&
						(time_stamp - status_start_time) >= 300)
					{
						if (status_fp != NULL)
						{
							fclose(status_fp);
							status_fp = NULL;
						}
					}
					if (status_fp == NULL)
					{						
						//DeviceID[4]_PID[4]_Year[4]Month[2]Day[2]_Hour[2]Min[2]Sec[2]
						struct tm tmBuf;
						if (localtime_s(&tmBuf, &time_stamp) != 0)
						{
							printf("Error Converting Time Stamp\n");
							return -1;
						}
						snprintf(status_file_name,
							FILE_BUFFER_SIZE,
							"%04d_%04d_%04d%02d%02d_%02d%02d%02d",
							device_id,
							pid,
							tmBuf.tm_year + 1900,
							tmBuf.tm_mon + 1,
							tmBuf.tm_mday,
							tmBuf.tm_hour,
							tmBuf.tm_min,
							tmBuf.tm_sec);
						
						strncat_s(status_file_name, FILE_BUFFER_SIZE, ".sta", 4);
						if (fopen_s(&status_fp, status_file_name, "w") != 0)
						{
							printf("Failed to open file:%s\n", status_file_name);
							return -1;
						}
						status_start_time = time_stamp;
					}
					else
					{
						fwrite(value_buffer, sizeof(uint8_t), value_size, status_fp);
					}

				}
				else if (key_type == Data_Record)
				{
					//If the start time is set and is greater than the current time stamp
					//by 5 minutes close the file so a new file can be started
					if ((data_start_time != 0) &&
						(time_stamp - data_start_time) >= 300)
					{
						if (data_fp != NULL)
						{
							fclose(data_fp);
							data_fp = NULL;
						}
					}
					if (data_fp == NULL)
					{						
						//DeviceID[4]_PID[4]_Year[4]Month[2]Day[2]_Hour[2]Min[2]Sec[2]
						struct tm tmBuf;
						if (localtime_s(&tmBuf, &time_stamp) != 0)
						{
							printf("Error Converting Time Stamp\n");
							return -1;
						}
						snprintf(data_file_name,
							(MAX_OUTPUT_PATH + MAX_FILE_NAME_LEN),
							"%04d_%04d_%04d%02d%02d_%02d%02d%02d",
							device_id,
							pid,
							tmBuf.tm_year + 1900,
							tmBuf.tm_mon + 1,
							tmBuf.tm_mday,
							tmBuf.tm_hour,
							tmBuf.tm_min,
							tmBuf.tm_sec);
						
						strncat_s(data_file_name, (MAX_OUTPUT_PATH + MAX_FILE_NAME_LEN), ".dat", 4);						
						if (fopen_s(&data_fp, data_file_name, "w") != 0)
						{
							printf("Failed to open file:%s\n", data_file_name);
							return -1;
						}
						data_start_time = time_stamp;
					}
					else
					{
						fwrite(value_buffer, sizeof(uint8_t), value_size, data_fp);
					}
				}
				else
				{
					printf("Unknown Record Type\n");
				}

#ifdef DEBUG
                fprintf(stderr, "Key Type: %d\n", key_type);
				fprintf(stderr, "Time Stamp: %lu\n", time_stamp);
				fprintf(stderr, "Sequence Number: %llu\n", sequence_num);
                
                fprintf(stderr, "\n");
                for (int x = 0; x < (value_size / sizeof(WaveFormDataBlock)); x++)
                {
					WaveFormDataBlock *db = (value_buffer + (x * sizeof(WaveFormDataBlock)));
					uint16_t header;
					fprintf(stderr, "WaveFormDataBlock\n");
					fprintf(stderr, "Header: %d\n", db->header);
					fprintf(stderr, "Serial Number: %d\n", db->serial_number);
					uint32_t rtc = (db->rtc_high_b << 16) & (db->rtc_mid_b << 8) & db->rtc_low_b;
					fprintf(stderr, "RTC: %ld\n", rtc);
					for (int waveform_index = 0; waveform_index < 7; waveform_index++)
					{
						fprintf(stderr, "Waveform: %d\n", waveform_index);
						fprintf(stderr, "ECG: [");
						for (int ecg_index = 0; ecg_index < 3; ecg_index++)
						{
							fprintf(stderr, "%d", db->wave_forms[waveform_index].ecg[ecg_index]);
						}
						fprintf(stderr, "]\n");
						fprintf(stderr, "PPG: [");
						for (int ppg_index = 0; ppg_index < 12; ppg_index++)
						{
							fprintf(stderr, "%d", db->wave_forms[waveform_index].ppg[ppg_index]);
						}
						fprintf(stderr, "]\n");
					}
                }
                fprintf(stderr, "\n");
#endif
            }
    }

	if (data_fp != NULL)
	{
		fclose(data_fp);
		data_fp = NULL;
	}
	if (status_fp != NULL)
	{
		fclose(status_fp);
		status_fp = NULL;
	}

	printf("Complete\n");

    return 0;
}
