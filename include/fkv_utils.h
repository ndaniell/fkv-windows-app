/** @file fkv_utils.h
*
* @brief This module implements utility functions.
*
* @par
* Copyright � 2016 Sunrise Labs, Inc.
* All rights reserved.
*/

#ifndef FKV_UTILS_H
#define FKV_UTILS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>  
  
typedef struct fkv_part_info_s
{
    uint64_t start_address;
    uint64_t size_bytes;
    uint8_t  part_type;
} fkv_part_info_t;
  
uint8_t fkv_fdisk(const uint32_t szt[]	/* Pointer to the size table for each partitions */
);

void fkv_mkfs(const uint8_t part_number);

void fkv_get_part_info(const uint8_t part_number, fkv_part_info_t* part_info);

#ifdef __cplusplus
}
#endif

#endif /* FKV_UTILS_H */
