/** @file fkv_bbwa_tree.h
*
* @brief This module implements the binary balacing tree layer of the SLI FKV Key Value storage manager.
*
* @par
* Copyright © 2016 Sunrise Labs, Inc.
* All rights reserved.
*/

/*
FORMERLY Before conversion for use in SLI FKV:
Copyright (c) 2012-16 Doug Currie, Londonderry, NH, USA

Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without 
restriction, including without limitation the rights to use, copy, modify, merge, publish, 
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef FKV_BBWA_TREE_H
#define FKV_BBWA_TREE_H

/*
the node fields:
1: key
2: val
3: size of this subtree
4: left
5: right
*/

#include <stdint.h>
#include "fkv_context.h"

fkv_noderef_t fkv_insert (fkv_context_t ctxt, fkv_noderef_t node, uint8_t *key, fkv_noderef_t val);

fkv_noderef_t fkv_lookup (fkv_context_t ctxt, fkv_noderef_t node, uint8_t *key, fkv_noderef_t nope);

fkv_noderef_t fkv_nafter (fkv_context_t ctxt, fkv_noderef_t node, uint8_t *key, fkv_noderef_t nope);

fkv_noderef_t fkv_indexth (fkv_context_t ctxt, fkv_noderef_t node, int32_t index, fkv_noderef_t nope);

fkv_noderef_t fkv_delete (fkv_context_t ctxt, fkv_noderef_t node, uint8_t *key);

#endif
