/** @file fkv_hal.h
*
* @brief This module implements the hardware abstraction layer of the SLI FKV Key Value storage manager.
*
* @par
* Copyright © 2016 Sunrise Labs, Inc.
* All rights reserved.
*/

#ifndef FKV_HAL_H
#define FKV_HAL_H

#ifdef __cplusplus
extern “C” {
#endif

#include "fkv_context.h"

typedef struct fkv_device_info_s
{
    uint64_t capacity_bytes;
} fkv_device_info_t;

void fkv_hal_open (void);

void fkv_hal_close (void);

void fkv_device_info (fkv_device_info_t *dev_info);

void fkv_hal_write(uint32_t *write_buffer, uint64_t write_buffer_size, const uint64_t write_address);

void fkv_hal_read(uint32_t *read_buffer, uint64_t read_buffer_size, const uint64_t read_address);

void fkv_hal_erase(const uint64_t erase_address, uint64_t erase_size);

void fkv_panic (void);

void fkv_hal_read_commit_record(fkv_commit_record_t *commit_rec);

void fkv_hal_write_commit_record(fkv_commit_record_t *commit_rec);

#ifdef __cplusplus
}
#endif

#endif /* FKV_HAL_H */
