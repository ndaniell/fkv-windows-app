/** @file fkv_context.h
*
* @brief This module implements the context structure of the SLI FKV Key Value storage manager.
*
* @par
* Copyright © 2016 Sunrise Labs, Inc.
* All rights reserved.
*/

#ifndef FKV_CONTEXT_H
#define FKV_CONTEXT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "fkv_api.h"

/* Configuration */
  
/* When FKV_OPTION_REMOVEABLE_RECOVERABLE is defined, several extra fields are added to the
* record header to support recovery of the commit record from just the partition data. This
* facilitates use of removeable media. 
*/
#define FKV_OPTION_REMOVEABLE_RECOVERABLE  

/* There are four options for tree node sizing, and this affects partitition size & performance
* 1. 32-byte node with 20-byte key and 24-bit noderefs
* 2. 64-byte node with 48-byte key and 31-bit noderefs
* 3. 64-byte node with 20-byte key and 31-bit noderefs (and 28-byte spare/unused area)
* 4. 32-byte node with 16-byte key and 31-bit noderefs
* Only options 1 & 3 are well tested.
* We say 31-byte noderefs; they are 32-bits, except that there are a few reserved values,
* so the full 2^32-1 space is not available. 
* The practical partition size maximums are:
* 24-bit noderefs with 32-byte node: 2^24 * 2^5 = 2^29 = 512 MiB
* 31-bit noderefs with 64-byte node: 2^31 * 2^6 = 2^37 = 128 GiB -- see note 
* 31-bit noderefs with 32-byte node: 2^31 * 2^5 = 2^36 =  64 GiB -- see note 
* Note: 32 GiB is the maximum size for SDHC cards in the SD 2.0 specification.
*
* With FKV_OPTION_31BIT_NODEREF,
* FKV_OPTION_SMALL_NODEKEY selects 16-byte keys
* FKV_OPTION_LARGE_NODEKEY selects 48-byte keys
* Otherwise, backward compatible 20-byte keys are used.
*/
#define FKV_OPTION_31BIT_NODEREF
//#define FKV_OPTION_SMALL_NODEKEY
//#define FKV_OPTION_LARGE_NODEKEY

#define FKV_PAGESIZE (4096u)

#define FKV_CACHESIZE (256u) /* in cache entries */

#define FKV_HASHTSIZE (2u * FKV_CACHESIZE) /* 50% load factor will give good hash table performance */

#define FKV_EVICT_SIZE (15u) /* evict nodes smaller than this size without looking exhaustively for mininum sized node */

// Select one of the following hash functions:
//#define FKV_USE_JENKINS (1)
//#define FKV_USE_MURMUR3 (1)
#define FKV_USE_FNV1A (1)

/* End of Configuration */

/* Index Tree Nodes */

typedef struct fkv_node_s
{
#if defined(FKV_OPTION_31BIT_NODEREF)
#if defined(FKV_OPTION_LARGE_NODEKEY)
    #define FKV_TREE_KEY_SIZE (48)
    uint8_t  key[FKV_TREE_KEY_SIZE]; 
#else
#if defined(FKV_OPTION_SMALL_NODEKEY)
    #define FKV_TREE_KEY_SIZE (16)
    uint8_t  key[FKV_TREE_KEY_SIZE]; 
#else
    #define FKV_TREE_KEY_SIZE (20)
    uint8_t  key[FKV_TREE_KEY_SIZE]; 
    uint8_t  unused[48 - FKV_TREE_KEY_SIZE];
#endif
#endif
    uint32_t val;    
    uint32_t left;    
    uint32_t right;  
    uint32_t size;
#else
    #define FKV_TREE_KEY_SIZE (20)
    uint8_t  key[FKV_TREE_KEY_SIZE];
    uint32_t size0:8;
    uint32_t val:24;
    uint32_t size1:8;
    uint32_t left:24;
    uint32_t size2:8;
    uint32_t right:24;
#endif    
} fkv_node_t;

#define FKV_NODES_PER_PAGE ((uint32_t)(FKV_PAGESIZE / sizeof(fkv_node_t)))

/* a fkv_noderef_t is an index relative to the start of the flash log in units of fkv_node_t
   so if a fkv_node_t is 32 bytes, it is multiplied by 32 to get a byte offset
*/
typedef uint32_t fkv_noderef_t;

/* A null node reference can be zero
   which is convenient for initialization of data structures
   and safe because blocks in the flash will have a header,
   so no node can start at zero.
*/
#define FKV_NULL_NODEREF (0u)

/* A deleted node reference is only used in the cache
   so it need not fit in a 24 bit fkv_node_t field.
*/
#define FKV_DELETED_NODEREF (0xFFFFFFFFu)

/* value that is too big for a 3-byte or a 31-bit noderef,
** so cannot be returned by fkv_lookup for a legit node 
*/
#define FKV_NONESUCH ((fkv_noderef_t )(0xFFFFFFFEu))

static inline bool fkv_noderef_is_null (const fkv_noderef_t noderef)
{
    return noderef == FKV_NULL_NODEREF;
}

static inline uint32_t fkv_node_size (const fkv_node_t *pnode)
{
#if defined(FKV_OPTION_31BIT_NODEREF)  
    return pnode->size;
#else
    return pnode->size0 + ((uint32_t )pnode->size1 << 8) + ((uint32_t )pnode->size2 << 16);
#endif    
}

static inline void fkv_set_node_size (fkv_node_t *pnode, const uint32_t size)
{
#if defined(FKV_OPTION_31BIT_NODEREF)  
    pnode->size = size;
#else  
    pnode->size0 =  size        & 0xFFu;
    pnode->size1 = (size >>  8) & 0xFFu;
    pnode->size2 = (size >> 16) & 0xFFu;
#endif    
}

/* Index Tree Cache */

typedef struct fkv_hash_bucket_s 
{
    fkv_noderef_t noderef;
    uint16_t cache_slot;
} fkv_bucket_t;


/* The fkv_partition_t describes the storage. The values must be persisted somewhere outside the
** fkv implementation. We leave this to the application code and FKV HAL to fill in the
** appropriate values.
*/
typedef struct fkv_commit_record_s
{
    uint32_t log_start_page;    // the index of the oldest live FKV_PAGESIZE page
    uint32_t next_node;         // for the allocator; at commit points one past the root node
#ifdef FKV_OPTION_REMOVEABLE_RECOVERABLE   
    uint64_t sequence_number;
#endif    
} fkv_commit_record_t;

struct fkv_context_s
{     
    uint8_t  page_buffer[FKV_PAGESIZE]; // written to end of flash log when page_buffer_is_dirty
    uint8_t  node_block_cache_populated;

    // these fields must be persisted somewhere to commit a transaction
    uint32_t base_page;         // the index of the starting FKV_PAGESIZE page
    uint32_t size_in_pages;     // the size of the fkv storage in FKV_PAGESIZE pages
    fkv_commit_record_t commit_record;
    
    //
    // these fields are ephemeral (do not need to be persisted)
    //
    uint32_t gc_wraparounds;        // number of times the garbage collector wrapped log_start_page back to zero
    uint64_t gc_records_expunged;   // number of records deleted by garbage collector
    uint64_t gc_records_forwarded;  // number of records copied by garbage collector
    //
    fkv_bucket_t fkv_hash_table[FKV_HASHTSIZE];
    fkv_node_t   fkv_node_cache[FKV_CACHESIZE];
    uint32_t     fkv_locked_cache[FKV_CACHESIZE / 32u];
    uint16_t     freelist_root; /* root is slot index into fkv_node_cache; freelist uses right link */
    uint16_t     eviction_indx; /* for size-based clock algorithm */
    //
    fkv_noderef_t tree_root;    // the root of the fkv tree
                                // from a commit record can be calculated as next_node-1
    //
    bool        page_buffer_is_dirty;
    //
    //int (*deep_compare)();    // TODO maybe
};

static inline void init_fkv_context (fkv_context_t ctxt)
{
    uint32_t i;
    memset(ctxt, 0u, sizeof(*ctxt)); /* zero is FKV_NULL_NODEREF */
    for (i = 0u; i < FKV_CACHESIZE; i++) ctxt->fkv_node_cache[i].right = i + 1; /* use right link for free list */
}

static inline uint16_t fkv_get_free_cache_slot (fkv_context_t ctxt)
{
    uint16_t res = ctxt->freelist_root; // the head of the free list
    if (res != FKV_CACHESIZE)
    {
        // pop the list
        ctxt->freelist_root = ctxt->fkv_node_cache[res].right;
    }
    return res;
}

static inline void fkv_put_free_cache_slot (fkv_context_t ctxt, uint16_t slot) // add slot to freelist
{
    ctxt->fkv_node_cache[slot].right = ctxt->freelist_root;
    ctxt->freelist_root = slot;    
}

static inline bool cache_entry_is_locked (fkv_context_t cntx, unsigned int index) {
    return 0 != (cntx->fkv_locked_cache[index / 32u] & (1u << (index % 32u)));
}

static inline void cache_entry_lock (fkv_context_t cntx, unsigned int index) {
    cntx->fkv_locked_cache[index / 32u] |= (1u << (index % 32u));
}

static inline void cache_entry_unlock (fkv_context_t cntx, unsigned int index) {
    cntx->fkv_locked_cache[index / 32u] &= ~(1u << (index % 32u));
}

static inline void fkv_wipe_cache (fkv_context_t cntx)
{
    int i;
    for (i = 0; i < FKV_CACHESIZE; i++) cntx->fkv_node_cache[i].right = i + 1; /* use right link for free list */
    for (i = 0; i < FKV_HASHTSIZE; i++) cntx->fkv_hash_table[i].noderef = FKV_NULL_NODEREF;
    cntx->freelist_root = 0u;
}

/* overall log-storage info */

static inline uint32_t fkv_page_number_of_page_buffer (fkv_context_t ctxt)
{
    /* next_node can be zero when we wrap the logstore; this is a problem for users of this function to deal with */
    return (((ctxt->commit_record.next_node == 0u) ? 0u : (ctxt->commit_record.next_node - 1u)) / FKV_NODES_PER_PAGE);
}

static inline uint32_t fkv_qty_free_pages (fkv_context_t ctxt)
{
    uint32_t pg_first = ctxt->commit_record.log_start_page;
    uint32_t pg_last  = fkv_page_number_of_page_buffer(ctxt);
    uint32_t pg_size  = ctxt->size_in_pages;
    uint32_t pg_free  = ((pg_size + pg_first) - pg_last) % pg_size; // add size so result of subtract remains positive
    //printf("fkv_qty_free_pages %u %u %u %u\n", pg_first, pg_last, pg_size, pg_free);
    return (pg_first == pg_last) ? pg_size : pg_free;
}

static inline bool fkv_gc_is_advisable (fkv_context_t ctxt) // not mandatory yet, but to spread out the work, do some gc now
{
    return (fkv_qty_free_pages(ctxt) < (ctxt->size_in_pages / 4u));
}

static inline bool fkv_gc_is_mandatory (fkv_context_t ctxt) // do gc now until fkv_gc_is_mandatory() returns false
{
    // the max key is 4k and max value is 64k; 
    // with tree size of 64k nodes, depth is 16, so we could have as many as 40 nodes in Index update; 
    // 4k + 64k + 40 * 32 ~= 72k -- double it to be safe
    return (fkv_qty_free_pages(ctxt) < ((144u * 1024u) / FKV_PAGESIZE));
}


#if 0
static inline int nlz32 (uint32_t x)
{
#ifdef __IAR_SYSTEMS_ICC__
    return (x == 0u) ? 32 : __CLZ((unsigned int )x);
#elif defined(_MSC_VER) && defined(_M_AMD64)
   unsigned long bitno;
   uint8_t s = _BitScanReverse(&bitno, (unsigned long )x);
   return s ? (31 - (int )bitno) : 32;
#elif defined(__GNUC__) && (UINT_MAX == 0xFFFFFFFFUL)
   return (x == 0u) ? 32 : __builtin_clz((unsigned int )x);
#elif defined(__GNUC__) && (ULONG_MAX == 0xFFFFFFFFUL)
   return (x == 0u) ? 32 : __builtin_clzl((unsigned long )x);
#else
   int n;
   if (x == 0) return(32);
   n = 0;
   if (x <= 0x0000FFFF) {n = n +16; x = x <<16;}
   if (x <= 0x00FFFFFF) {n = n + 8; x = x << 8;}
   if (x <= 0x0FFFFFFF) {n = n + 4; x = x << 4;}
   if (x <= 0x3FFFFFFF) {n = n + 2; x = x << 2;}
   if (x <= 0x7FFFFFFF) {n = n + 1;}
   return n;
#endif
}
#endif

#ifdef __cplusplus
}
#endif

#endif /* FKV_CONTEXT_H */
