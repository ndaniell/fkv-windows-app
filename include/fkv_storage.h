/** @file fkv_storage.h
*
* @brief This module implements the storage layer of the SLI FKV Key Value storage manager.
*
* @par
* Copyright © 2016 Sunrise Labs, Inc.
* All rights reserved.
*/

#ifndef FKV_STORAGE_H
#define FKV_STORAGE_H

#ifdef __cplusplus
extern “C” {
#endif

#include <stdint.h>
  
#include "fkv_context.h"
  
#define FKV_RECORD_SIGNATURE 0x494c5365  

/* By limiting the key_size to < 4 KiBytes we
** a) keep the size of this header to 4 bytes including 4 bits for flags
** b) keep the key on the first page;
**       which simplifies the GC, which needs to construct the tree key
** We have an extra bit in flags, should we have a need for expanding the header.
*/
typedef struct fkv_record_header_s
{
    uint16_t flags:4;       /* bitset of values FKV_FLAG_xxx */
    uint16_t key_size:12;   /* number of bytes in key */
    uint16_t val_size;      /* number of bytes in value */
    /* would like to include the Index size, but it's not known when the 1st page is written */
#ifdef FKV_OPTION_REMOVEABLE_RECOVERABLE
    uint32_t signature;     /* a constant *(uint32_t *)"eSLI" ? = 0x494c5365 little endian */
    fkv_commit_record_t commit_record; /* Commit record prior to writing this record */
#endif    
} fkv_record_header_t;

#if (FKV_PAGESIZE <= 4096u)
#define FKV_MAX_KEY_SIZE (FKV_PAGESIZE - sizeof(fkv_record_header_t))
#else
#define FKV_MAX_KEY_SIZE (       4096u - sizeof(fkv_record_header_t))
#endif

#define FKV_ERASE_SIZE (FKV_PAGESIZE * 4)
#define FKV_BLOCK_SIZE (512u)  
#define FKV_MAX_FLAGS_VALUE (0x0fu)
#define FKV_COMMIT_RECORD_ADDRESS (512u)

fkv_noderef_t
fkv_allocate_node (fkv_context_t ctxt
                 , uint8_t *key             /* in */
                 , fkv_noderef_t val        /* in */
                 , uint32_t size            /* in */
                 , fkv_noderef_t left       /* in */
                 , fkv_noderef_t right      /* in */
                 );

void
fkv_read_node     (fkv_context_t ctxt
                 , fkv_node_t *pnode_target /* out; size of buffer is implicitly sizeof(fkv_node_t) */
                 , fkv_noderef_t noderef    /* in */
                 );

fkv_noderef_t
fkv_allocate_kvir (fkv_context_t ctxt
                 , uint8_t *key             /* in */
                 , uint16_t key_size        /* in */
                 , uint16_t flags           /* in; flags are needed so we know how to convert Key to tree key */
                 , uint8_t *value           /* in */
                 , uint16_t value_size      /* in */
                 );

void
fkv_read_kvir    (fkv_context_t ctxt
                 , uint8_t  *key            /* out; NULL pointer ok if key not wanted */
                 , uint16_t *key_size       /* in/out; in: size of key buffer, zero ok; out: size of key */
                 , uint8_t  *value          /* out; NULL pointer ok if value not wanted */
                 , uint16_t *value_size     /* in/out; in: size of value buffer, zero ok; out: size of value */
                 , fkv_noderef_t noderef    /* in */
                 );

void fkv_commit (fkv_context_t ctxt);

#ifdef FKV_OPTION_REMOVEABLE_RECOVERABLE

int8_t fkv_commit_recovery (fkv_context_t ctxt);

void fkv_page_fill(fkv_context_t ctxt);

#endif

uint32_t fkv_gc_one_record (fkv_context_t ctxt); /* returns number of pages reclaimed (may be zero) */

void fkv_process_key (uint8_t *treekey, uint8_t *key, uint16_t key_size, uint16_t flags);

#ifdef __cplusplus
}
#endif

#endif /* FKV_STORAGE_H */
