/** @file fkv_api.h
*
* @brief This module implements the API the SLI FKV Key Value storage manager.
*
* @par
* Copyright © 2016 Sunrise Labs, Inc.
* All rights reserved.
*/

#ifndef FKV_API_H
#define FKV_API_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef struct fkv_context_s *fkv_context_t;

#define FKV_FLAG_PERSISTANT  (0x00)  /* the garbage collector may not delete this record to make room -- the default */
#define FKV_FLAG_EXPUNGEABLE (0x01)  /* the garbage collector may delete this record to make room */
#define FKV_FLAG_PREFIX_KEY  (0x00)  /* the 20-byte prefix of the full Key is the tree key -- the default */
#define FKV_FLAG_SHA_1_KEY   (0x02)  /* SHA-1 hash is used to convert full Key to tree key */

typedef enum fkv_status_e
{
    FKV_OK,
    FKV_NOT_FOUND,
    FKV_MEDIA_ERROR,
    FKV_API_ARG_ERROR,
    FKV_COMMIT_RECORD_RECOVERY_ERROR
} fkv_status_t;


// partition - The partition in the MBR where the FKV partition should be created
fkv_status_t fkv_create (fkv_context_t *ctxt, unsigned int partition);

// partition - The partition in the MBR where the FKV partition should be used
// recovery - 0 indicates that the commit record should be recieved via the hal. Any value other than 0 indicates the commit record should be recovered from flash
fkv_status_t fkv_open (fkv_context_t *ctxt, unsigned int partition, const uint8_t recovery);    

fkv_status_t 
fkv_put (fkv_context_t ctxt,
         uint8_t *key,          /* in */
         uint16_t key_size,     /* in */
         uint16_t flags,        /* in; flags are needed so we know how to convert Key to tree key */
         uint8_t *value,        /* in */
         uint16_t value_size    /* in */
        );

/* fkv_get() gets the record with Key
** For value_size and keyout_size, if the value's (key's) size exceeds the value_size (keyout_size) arg, then
** the first value_size (keyout_size) bytes are copied, but the return value is the full value's (key's) size.
** Note that the keyout may differ from key depending on the flags, e.g., FKV_FLAG_PREFIX_KEY.
*/
fkv_status_t 
fkv_get (fkv_context_t ctxt,
         uint8_t  *key,         /* in */
         uint16_t  key_size,    /* in */
         uint16_t  flags,       /* in; flags are needed so we know how to convert Key to tree key */
         uint8_t  *value,       /* out; NULL pointer ok if value not wanted */
         uint16_t *value_size,  /* in/out; in: size of value buffer, zero ok; out: size of value */
         uint8_t  *keyout,      /* out; NULL pointer ok if keyout not wanted */
         uint16_t *keyout_size  /* in/out; in: size of keyout buffer, zero ok; out: size of key */
        );

/* fkv_nxt() gets the record next after Key in lexicographical order (using memcmp)
** For value_size and keyout_size, if the value's (key's) size exceeds the value_size (keyout_size) arg, then
** the first value_size (keyout_size) bytes are copied, but the return value is the full value's (key's) size.
** Note that the keyout may be used as the key argument for the subsequent call to fkv_nxt() to sequence through the 
** records in lexicographical order (using memcmp).
*/
fkv_status_t 
fkv_nxt (fkv_context_t ctxt,
         uint8_t  *key,         /* in */
         uint16_t  key_size,    /* in */
         uint16_t  flags,       /* in; flags are needed so we know how to convert Key to tree key */
         uint8_t  *value,       /* out; NULL pointer ok if value not wanted */
         uint16_t *value_size,  /* in/out; in: size of value buffer, zero ok; out: size of value */
         uint8_t  *keyout,      /* out; NULL pointer ok if keyout not wanted */
         uint16_t *keyout_size  /* in/out; in: size of keyout buffer, zero ok; out: size of key */
        );

/* fkv_nth() returns the indexth item in key order when index >= 0
** fkv_nth() returns the treesize + indexth item in key order when index < 0
** fkv_nth() returns FKV_NOT_FOUND when index exceeds the treesize
*/
fkv_status_t 
fkv_nth (fkv_context_t ctxt,
         uint32_t  index,       /* in */
         uint8_t  *value,       /* out; NULL pointer ok if value not wanted */
         uint16_t *value_size,  /* in/out; in: size of value buffer, zero ok; out: size of value */
         uint8_t  *keyout,      /* out; NULL pointer ok if keyout not wanted */
         uint16_t *keyout_size  /* in/out; in: size of keyout buffer, zero ok; out: size of key */
        );

/* fkv_rem() removes the record with Key from the Index
** It does so by appending a record to the log with no value and FKV_FLAG_EXPUNGEABLE set. 
*/
fkv_status_t 
fkv_rem (fkv_context_t ctxt,
         uint8_t *key,          /* in */
         uint16_t key_size,     /* in */
         uint16_t flags         /* in; flags are needed so we know how to convert Key to tree key */
        );

fkv_status_t fkv_close (fkv_context_t ctxt);

#ifdef __cplusplus
}
#endif

#endif /* FKV_API_H */
