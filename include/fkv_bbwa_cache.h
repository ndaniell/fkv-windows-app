/** @file fkv_bbwa_cache.h
*
* @brief This module implements the cache for SLI FKV Key Value storage manager.
*
* @par
* Copyright © 2016 Sunrise Labs, Inc.
* All rights reserved.
*/

#ifndef FKV_BBWA_CACHE_H
#define FKV_BBWA_CACHE_H

#include "fkv_bbwa_tree.h"

/* fkv_cache_node_locked never returns NULL
   the node is locked, so it will not be evicted until it is unlocked
*/
fkv_node_t *fkv_cache_node_locked(fkv_context_t ctxt, fkv_noderef_t noderef);

/* fkv_cache_node_unlocked never returns NULL
   the node is not locked, so it may be evicted by another fkv_cache_ call
*/
fkv_node_t *fkv_cache_node_unlocked(fkv_context_t ctxt, fkv_noderef_t noderef);

/* node may be evicted from cache
*/
void fkv_unlock_node(fkv_context_t ctxt, fkv_noderef_t noderef, fkv_node_t *node);

/* node will be evicted from cache
   fkv_uncache_node() does an implicit unlock
*/
void fkv_uncache_node(fkv_context_t ctxt, fkv_noderef_t noderef, fkv_node_t *node);

static inline uint32_t fkv_cache_chain_length (uint32_t start, uint32_t end)
{
    return ((end + FKV_CACHESIZE) - start) % FKV_CACHESIZE;
}

#endif
